<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cake Template">
    <meta name="keywords" content="Cake, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cake | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:wght@400;500;600;700;800;900&display=swap"
    rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/barfiller.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/btn-wa.css" type="text/css">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/cake-main/img/rotina_logo/rotina_ico.ico">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link href="<?php echo base_url();?>assets/summernote/summernote.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/summernote/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <!-- <script src="<?php echo base_url();?>assets/Chart/Chart.js"></script> -->
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> -->
  
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Offcanvas Menu Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        <div class="offcanvas__cart">
            <div class="offcanvas__cart__links">
                <a href="#" class="search-switch"><img src="<?php echo base_url();?>assets/cake-main/img/icon/search.png" alt=""></a>
                <a href="#"><img src="<?php echo base_url();?>assets/cake-main/img/icon/heart.png" alt=""></a>
            </div>
            <div class="offcanvas__cart__item">
                <a href="#"><img src="<?php echo base_url();?>assets/cake-main/img/icon/cart.png" alt=""> <span>0</span></a>
                <div class="cart__price">Cart: <span>$0.00</span></div>
            </div>
        </div>
        <div class="offcanvas__logo">
            <a href="#"><img src="<?php echo base_url();?>assets/cake-main/img/rotina_logo/logo.png" alt=""></a>
        </div>
    </div>
    <!-- Offcanvas Menu End -->

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="header__top__inner">
                            <div class="header__top__left">
                            </div>
                            <div class="header__logo">
                                <a href="<?php echo base_url('admin');?>"><img src="<?php echo base_url();?>assets/cake-main/img/rotina_logo/logoo.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <?php
                            if ($kode == 'dashboard') { ?>
                            <li class="active"><a href="<?php echo base_url().'admin/dashboard';?>">Dashboard</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'admin/dashboard';?>">Dashboard</a></li>
                            <?php } ?>

                            <?php
                            if ($kode == 'produk') { ?>
                            <li class="active"><a href="<?php echo base_url().'admin';?>">Produk</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'admin';?>">Produk</a></li>
                            <?php } ?>

                            <?php
                            if ($kode == 'kategori') { ?>
                            <li class="active"><a href="<?php echo base_url().'admin/kategori';?>">Kategori</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'admin/kategori';?>">Kategori</a></li>
                            <?php } ?>

                            <?php
                            if ($kode == 'promo') { ?>
                            <li class="active"><a href="<?php echo base_url().'admin/promo';?>">Promo</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'admin/promo';?>">Promo</a></li>
                            <?php } ?>

                            <?php
                            if ($kode == 'konten') { ?>
                            <li class="active"><a href="<?php echo base_url().'admin/konten';?>">Konten Website</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'admin/konten';?>">Konten Website</a></li>
                            <?php } ?>

                            <?php
                            if ($kode == 'pesanan') { ?>
                            <li class="active"><a href="<?php echo base_url().'admin/pesanan';?>">Pesanan</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'admin/pesanan';?>">Pesanan</a></li>
                            <?php } ?>

                            <li><a href="<?php echo base_url().'admin/logout';?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/logout.png" alt="Logout"></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Section End -->
    <a href="https://api.whatsapp.com/send?phone=6285659272742" onclick="topFunction()" id="myBtn" title="Order"><img src="<?php echo base_url();?>assets/cake-main/img/wa.png"></a>