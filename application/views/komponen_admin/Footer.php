 <footer class="footer set-bg" data-setbg="<?php echo base_url();?>assets/cake-main/img/footer-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer__widget">
                        <h6>WORKING HOURS</h6>
                        <ul>
                            <li>Monday - Saturday : 08:00 am – 12:00 pm</li>
                            <li>Sunday : 08:00 am – 16:00 pm</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__logo">
                            <a href="#"><img src="<?php echo base_url();?>assets/cake-main/img/rotina_logo/logo.png" alt=""></a>
                        </div>
                        <p>ROTINA isi enakkk We made from the heart.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer__newslatter">
                        <h6>Subscribe</h6>
                        <p>Get latest updates and offers.</p>
                        <form action="#">
                            <input type="text" placeholder="Email">
                            <button type="submit"><i class="fa fa-send-o"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <p class="copyright__text text-white"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                          This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                      </p>
                  </div>
                  <div class="col-lg-5">
                    <div class="copyright__widget">
                        <ul>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Site Map</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->

<!-- Search Begin -->
<div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch">+</div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
</div>
<!-- Search End -->

<!-- Js Plugins -->

<script src="<?php echo base_url();?>assets/summernote/summernote.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.barfiller.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.slicknav.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/main.js"></script>


<script type="text/javascript">
     // Start jQuery function after page is loaded
        $(document).ready(function(){
         // Initiate DataTable function comes with plugin
         $('#tabeluser').DataTable({
            "pageLength": 5,
         });
         $('#tabeluser2').DataTable({
            "pageLength": 5,
            });
         });

        $(document).ready(function(){    
         $("body").on("click", ".view_data", function(event){ 
          var produkData = $(this).attr('id');
                console.log(produkData)
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataProdukModal",
                    method: "POST",
                    data: {produkData:produkData},
                    success: function(data){
                        $('#produkHasil').html(data);
                        $('#produkModal').modal('show');
                    }
             });
         });
        });

        $(document).ready(function(){    
         $("body").on("click", ".view_data", function(event){ 
          var produkData = $(this).attr('id');
                console.log(produkData)
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataProdukModal",
                    method: "POST",
                    data: {produkData:produkData},
                    success: function(data){
                        $('#produkHasil').html(data);
                        $('#produkModal').modal('show');
                    }
             });
         });
        });

        $(document).ready(function(){    
         $("body").on("click", ".view_data_kategori", function(event){ 
          var idKategori = $(this).attr('id');
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataKategoriModal",
                    method: "POST",
                    data: {idKategori:idKategori},
                    success: function(data){
                        $('#kategoriHasil').html(data);
                        $('#kategoriModal').modal('show');
                    }
             });
         });
        });

        $(document).ready(function(){    
         $("body").on("click", ".view_data_promo", function(event){ 
          var idPromo = $(this).attr('id');
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataPromoModal",
                    method: "POST",
                    data: {idPromo:idPromo},
                    success: function(data){
                        $('#promoHasil').html(data);
                        $('#promoModal').modal('show');
                    }
             });
         });
        });

        $(document).ready(function(){    
         $("body").on("click", ".view_data_instagram", function(event){ 
          var idInstagram = $(this).attr('id');
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataInstagramModal",
                    method: "POST",
                    data: {idInstagram:idInstagram},
                    success: function(data){
                        $('#instagramHasil').html(data);
                        $('#instagramModal').modal('show');
                    }
             });
         });
        });

        $(document).ready(function(){    
         $("body").on("click", ".view_data_team", function(event){ 
          var idTeam = $(this).attr('id');
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataTeamModal",
                    method: "POST",
                    data: {idTeam:idTeam},
                    success: function(data){
                        $('#teamHasil').html(data);
                        $('#teamModal').modal('show');
                    }
             });
         });
        });

        $(document).ready(function(){    
         $("body").on("click", ".view_data_pembayaran", function(event){ 
          var idPesanan = $(this).attr('id');
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataPembayaranModal",
                    method: "POST",
                    data: {idPesanan:idPesanan},
                    success: function(data){
                        $('#pembayaranHasil').html(data);
                        $('#pembayaranModal').modal('show');
                    }
             });
         });
        });

        $(document).ready(function(){    
         $("body").on("click", ".view_data_pembayaran", function(event){ 
          var idPesanan = $(this).attr('id');
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataPembayaranModal",
                    method: "POST",
                    data: {idPesanan:idPesanan},
                    success: function(data){
                        $('#pembayaranHasil').html(data);
                        $('#pembayaranModal').modal('show');
                    }
             });
         });
        });

        $(document).ready(function(){    
         $("body").on("click", ".view_data_pembelian", function(event){ 
          var idPembelian = $(this).attr('id');
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataPembelianModal",
                    method: "POST",
                    data: {idPembelian:idPembelian},
                    success: function(data){
                        $('#pembelianHasil').html(data);
                        $('#pembelianModal').modal('show');
                    }
             });
         });
        });

        $(document).ready(function(){    
         $("body").on("click", ".view_data_lokasi", function(event){ 
          var id = $(this).attr('id');
                $.ajax({
                    url: "<?php echo base_url() ?>admin/getDataLokasiModal",
                    method: "POST",
                    data: {id:id},
                    success: function(data){
                        $('#lokasiHasil').html(data);
                        $('#lokasiModal').modal('show');
                    }
             });
         });
        });

        var max = document.getElementById('max');
        var min = document.getElementById('min');
        console.log(max);
        if (max) {
            max.addEventListener('keyup', function(e){
                max.value = formatRupiah(this.value, 'Rp. ');
            });
        }

        if (min) {
            min.addEventListener('keyup', function(e){
                min.value = formatRupiah(this.value, 'Rp. ');
            });          
        }
 
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
 
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
 
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

         $(document).ready(function() {
            $('#summernote').summernote({
              height: "300px",
              styleWithSpan: false
            });
            $('#summernote2').summernote({
              height: "300px",
              styleWithSpan: false
            });
            $('#summernote3').summernote({
              height: "300px",
              styleWithSpan: false
            });
            $('.summernotePromo').summernote({
              height: "300px",
              styleWithSpan: false
            });
            $('#summernotePromo2').summernote({
              height: "300px",
              styleWithSpan: false
            });
          }); 

        $('.date-own').datepicker({
         minViewMode: 2,
         format: 'yyyy'
       });
         // ==========================================================================
        
    </script>

</body>

</html>