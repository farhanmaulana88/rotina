<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Cake Template">
    <meta name="keywords" content="Cake, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cake | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:wght@400;500;600;700;800;900&display=swap"
    rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/barfiller.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/btn-wa.css" type="text/css">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/cake-main/img/rotina_logo/rotina_ico.ico">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
</head>
<style> 
div.ex1 {
  height: 500px;
  width: 282px;
  overflow-y: scroll;
}
</style>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Offcanvas Menu Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        <?php if ($this->session->userdata('status') != 'login') { ?>
            <div class="offcanvas__cart">
                <div class="offcanvas__cart__links">
                    <a href="#" data-toggle="modal" class="loginModal" data-target="#loginModal"><img src="<?php echo base_url();?>assets/cake-main/img/icon/user2.png" alt=""></a>
                </div>
                <div class="offcanvas__cart__item">
                   <a href="<?php echo base_url('main/formCart');?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/cart.png" alt=""> <span id="valueCart2">0</span></a>
                   <div class="cart__price">Cart: <span id="subtotalCart2">0.00</span></div>
                </div>
            </div>
        <?php }else{ ?>
            <div class="offcanvas__cart">
                <div class="offcanvas__cart__links">
                    <a href="#" class="view_data_bill" id="<?php echo $this->session->userdata('id'); ?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/bill.png" alt=""></a>
                    <a href="#" class="view_data_list" id="<?php echo $this->session->userdata('id'); ?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/list.png" alt=""></a>
                    <a href="#" class="view_data_user" id="<?php echo $this->session->userdata('id'); ?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/user2.png" alt=""></a>
                </div>
                <div class="offcanvas__cart__item">
                   <a href="<?php echo base_url('main/formCart');?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/cart.png" alt=""> <span id="valueCart2">0</span></a>
                   <div class="cart__price">Cart: <span id="subtotalCart2">0.00</span></div>
                </div>
            </div>
        <?php } ?>
        <div class="offcanvas__logo">
           <a href="<?php echo base_url('main');?>"><img src="<?php echo base_url();?>assets/cake-main/img/rotina_logo/logoo.png" alt=""></a>
        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
    <!-- Offcanvas Menu End -->

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="header__top__inner">
                            <div class="header__top__left">
                                <ul>
                                    <li><a href="http://facebook.com/<?= $social_media->facebook;?>"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="http://twitter.com/<?= $social_media->twitter;?>"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="http://instagram.com/<?= $social_media->instagram;?>"><i class="fa fa-instagram"></i></a></li>
                                    <?php if (!empty($this->session->userdata('id'))) { ?>
                                        <li>
                                            <?= $nama_pembeli->nama;?>
                                        </li>
                                    <?php }else{ ?>

                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="header__logo">
                                <a href="<?php echo base_url('main');?>"><img src="<?php echo base_url();?>assets/cake-main/img/rotina_logo/logoo.png" alt=""></a>


                            </div>
                            <?php if ($this->session->userdata('status') != 'login') { ?>
                                <div class="header__top__right">
                                    <div class="header__top__right__links">
                                        <a href="#" data-toggle="modal" class="loginModal" data-target="#loginModal"><img src="<?php echo base_url();?>assets/cake-main/img/icon/user2.png" alt=""></a>
                                    </div>
                                    <div class="header__top__right__cart">
                                        <a href="<?php echo base_url('main/formCart');?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/cart.png" alt=""> <span id="valueCart">0</span></a>
                                        <div class="cart__price">Cart: <span id="subtotalCart">0.00</span></div>
                                    </div>
                                </div>
                            <?php }else{ ?>
                                 <div class="header__top__right">
                                    <div class="header__top__right__links">
                                        <a href="#" class="view_data_bill" id="<?php echo $this->session->userdata('id'); ?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/bill.png" alt=""></a>
                                        <a href="#" class="view_data_list" id="<?php echo $this->session->userdata('id'); ?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/list.png" alt=""></a>
                                        <a href="#" class="view_data_user" id="<?php echo $this->session->userdata('id'); ?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/user2.png" alt=""></a>
                                    </div>
                                    <div class="header__top__right__cart">
                                        <a href="<?php echo base_url('main/formCart');?>"><img src="<?php echo base_url();?>assets/cake-main/img/icon/cart.png" alt=""> <span id="valueCart">0</span></a>
                                        <div class="cart__price">Cart: <span id="subtotalCart">0.00</span></div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <?php
                            if ($kode == 'index') { ?>
                            <li class="active"><a href="<?php echo base_url().'main';?>">Home</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'main';?>">Home</a></li>
                            <?php } ?>

                            <?php
                            if ($kode == 'produk') { ?>
                            <li class="active"><a href="<?php echo base_url().'main/produk';?>">Produk</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'main/produk';?>">Produk</a></li>
                            <?php } ?>

                            <?php
                            if ($kode == 'promo') { ?>
                            <li class="active"><a href="<?php echo base_url().'main/promo';?>">Promo</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'main/promo';?>">Promo</a></li>
                            <?php } ?>

                            <?php
                            if ($kode == 'tentang') { ?>
                            <li class="active"><a href="<?php echo base_url().'main/tentang';?>">Tentang Kami</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'main/tentang';?>">Tentang Kami</a></li>
                            <?php } ?>

                            <?php
                            if ($kode == 'kontak') { ?>
                            <li class="active"><a href="<?php echo base_url().'main/kontak';?>">Hubungi Kami</a></li>
                            <?php }else{ ?>
                            <li><a href="<?php echo base_url().'main/kontak';?>">Hubungi Kami</a></li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Modal Login -->
    <div id="loginModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title">Login Rotina</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body">
          <form method="post" action="<?php echo base_url('main/cek_login');?>">
            <center><img src="<?php echo base_url();?>assets/cake-main/img/rotina_logo/logo.png" alt=""></center>
            <div class="form-group" align="left">
              <label for="username"><img src="<?php echo base_url();?>assets/cake-main/img/icon/user.png"> Username:</label>
              <input type="text" class="form-control" id="username" placeholder="username" name="username" required>
            </div>
            <div class="form-group" align="left">
              <label for="password"><img src="<?php echo base_url();?>assets/cake-main/img/icon/password.png"> Password:</label>
              <input type="password" class="form-control" id="password" placeholder="password" name="password" required>
            </div>
          <!-- <span >Belum miliki akun? <a href="#" id="registrasi" class="registrasi">Klik Disini</a></span> -->
          </div>

          <div class="modal-footer">
            <button type="submit" name="login" class="btn btn-dark">LOGIN</button>
          </div>

          </form>
        </div>
      </div>
    </div>

    <!-- ============================================================================================ -->
    <!-- Modal Login -->
    <!-- Modal User -->
    <div id="registrasiModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Registrasi</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body">

            <div class="ex1">
                <form method="post" action="<?php echo base_url('main/registrasi');?>" enctype="multipart/form-data">
                    <center><img src="<?php echo base_url('assets/cake-main/img/rotina_logo/logo.png');?>"></center>
                    <center><img style="width:150px; height:90px;" src="<?php echo base_url('assets/cake-main/img/rotina_logo/member.png');?>"><br><br>
                    <div class="form-group" align="left">
                      <label><img src="<?php echo base_url('assets/cake-main/img/icon/user.png');?>"> Username:</label><br>
                      <input type="text" class="form-control" name="username" required>
                    </div>
                    <div class="form-group" align="left">
                      <label><img src="<?php echo base_url('assets/cake-main/img/icon/password.png');?>"> Password:</label>
                      <input type="password" class="form-control" name="password" required>
                    </div>
                    <div class="form-group" align="left">
                      <label><img src="<?php echo base_url('assets/cake-main/img/icon/nama.png');?>"> Nama Lengkap:</label>
                      <input type="text" class="form-control" name="nama" required>
                    </div>
                    <div class="form-group" align="left">
                      <label><img src="<?php echo base_url('assets/cake-main/img/icon/alamat.png');?>"> Alamat Lengkap:</label>
                      <textarea style="height:100px;" class="form-control" name="alamat" required></textarea>
                    </div>
                    <div class="form-group" align="left">
                      <label><i class="fa fa-picture-o" aria-hidden="true"></i> Foto:</label>
                      <input type="file" class="form-control foto" id="foto" name="foto" required>
                    </div>
                    <div class="modal-footer">
                      <a href="#" class="modalLogin"><i class="fa fa-chevron-left"></i></i></a>
                      <button type="submit" class="btn btn-dark">Daftar</button>
                    </div>
                </form>
            </div>
          
          </div>
        </div>
      </div>
    </div>
<!-- ================================================================================================================================================= -->
    <!-- Modal User -->
    <div id="userModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Profil Pembeli</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <div class="modal-body">

            <div id="userHasil"></div>
          
          </div>
        </div>
      </div>
    </div>

        <!-- Modal Cart -->
    <!-- Update Modal -->
    <div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="cartModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="cartHasil"></div>

        </div>
      </div>
     </div>
    </div>

    <!-- Modal List -->
    <div class="modal fade" id="listModal" tabindex="-1" role="dialog" aria-labelledby="listModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="listModalLabel">
                <img src="<?php echo base_url();?>assets/cake-main/img/rotina_logo/logoo.png" alt="">
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="listHasil"></div>

        </div>
      </div>
     </div>
    </div>

    <!-- Modal Bill -->
    <div class="modal fade" id="billModal" tabindex="-1" role="dialog" aria-labelledby="billModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="billModalLabel">
                <img src="<?php echo base_url();?>assets/cake-main/img/rotina_logo/logoo.png" alt="">
                </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="billHasil"></div>

        </div>
      </div>
     </div>
    </div>
    <!-- Header Section End -->
    <a href="https://api.whatsapp.com/send?phone=6285659272742" onclick="topFunction()" id="myBtn" title="Order"><img src="<?php echo base_url();?>assets/cake-main/img/wa.png"></a>
    <style type="text/css">
        .more {
          cursor:pointer;
        }
    </style>