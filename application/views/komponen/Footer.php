    <!-- Footer Section Begin -->
    <footer class="footer set-bg" data-setbg="<?php echo base_url();?>assets/cake-main/img/footer-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer__widget">
                        <h6>WORKING HOURS</h6>
                        <ul>
                            <li>Monday - Saturday : 08:00 am – 12:00 pm</li>
                            <li>Sunday : 08:00 am – 16:00 pm</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__logo">
                            <a href="#"><img src="<?php echo base_url();?>assets/cake-main/img/rotina_logo/logo.png" alt=""></a>
                        </div>
                        <p>ROTINA isi enakkk We made from the heart.</p>
                        <div class="footer__social">
                            <a href="http://facebook.com/<?= $social_media->facebook;?>"><i class="fa fa-facebook"></i></a>
                            <a href="http://twitter.com/<?= $social_media->twitter;?>"><i class="fa fa-twitter"></i></a>
                            <a href="http://instagram.com/<?= $social_media->instagram;?>"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer__newslatter">
                        <h6>Subscribe</h6>
                        <p>Get latest updates and offers.</p>
                        <form action="#">
                            <input type="text" placeholder="Email">
                            <button type="submit"><i class="fa fa-send-o"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <p class="copyright__text text-white"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                          This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                      </p>
                  </div>
                  <div class="col-lg-5">
                    <div class="copyright__widget">
                        <ul>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Site Map</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->

<!-- Search Begin -->
<div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch">+</div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Search here.....">
        </form>
    </div>
</div>
<!-- Search End -->

<!-- Js Plugins -->
<script src="<?php echo base_url();?>assets/cake-main/js/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.barfiller.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.slicknav.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url();?>assets/cake-main/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
      $('#tabeluser').DataTable();
  });
  </script>
<script type="text/javascript">
//Get the button:
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

 $(document).ready(function(){
    $('.view_data_user').click(function(){
        var idUser = $(this).attr('id');
        $.ajax({
            url: "<?php echo base_url() ?>main/getDataUserModal",
            method: "POST",
            data: {idUser:idUser},
            success: function(data){
                $('#userHasil').html(data);
                $('#userModal').modal('show');
                $('#cartModal').modal('hide');
            }
     });
 });
});

    $(document).ready(function(){
    $('.view_data_list').click(function(){
        var id = $(this).attr('id');
        $.ajax({
            url: "<?php echo base_url() ?>main/getDataListModal",
            method: "POST",
            data: {id:id},
            success: function(data){
                $('#listHasil').html(data);
                $('#listModal').modal('show');
            }
     });
 });
});

    $(document).ready(function(){
    $('.view_data_bill').click(function(){
        var id = $(this).attr('id');
        $.ajax({
            url: "<?php echo base_url() ?>main/getDataBillModal",
            method: "POST",
            data: {id:id},
            success: function(data){
                $('#billHasil').html(data);
                $('#billModal').modal('show');
            }
     });
 });
});

  $(document).ready(function(){
    $('.modalLogin').click(function(){
      $('#loginModal').modal('show');
                $('#registrasiModal').modal('hide');
            });
     });

  $('#registrasi').click(function() {
        $('#loginModal').modal('hide');
        $('#registrasiModal').modal('show');
  });


 $(document).ready(function(){
    $('.view_data_cart').click(function(){
        $.ajax({
            url: "<?php echo base_url() ?>main/getDataCartModal",
            method: "POST",
            data: {},
            success: function(data){
                $('#cartHasil').html(data);
                $('#cartModal').modal('show');
            }
     });
 });
});

 $('#valueCart').load("<?php echo base_url();?>main/countCart2");
 $('#subtotalCart').load("<?php echo base_url();?>main/subtotalCart2");

 $(document).ready(function(){
    $('.input_cart').click(function(){
        var idProduk = $(this).attr('id');
        $.ajax({
            url: "<?php echo base_url() ?>main/add_to_cart",
            method: "POST",
            data: {idProduk:idProduk},
            success: function(data){
                let res = JSON.parse(data);
                console.log(res.jumlah_harga);
                $('#valueCart').html(res.jumlah_barang);
                $('#subtotalCart').html(res.jumlah_harga);
                $('#valueCart2').html(res.jumlah_barang);
                $('#subtotalCart2').html(res.jumlah_harga);
                Command: toastr["success"]("Menambahkan " + res.data.name + " kedalam keranjang")

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
            }
     });
 });
});

$(document).on('click','.updateCart',function(){
        var rowid    = $('#rowid').val();
        console.log(rowid);

        $.ajax({
            url : "<?php echo base_url();?>main/updateCart",
            method : "POST",
            data : {rowid:rowid},
            success: function(data){
                $('#cartHasil').html(data);
                $('#cartModal').modal('show');
            }
 });
});

 $(document).on('click','.deleteCart',function(){
            var row_id = $(this).attr("id"); //mengambil row_id dari artibut id
            $.ajax({
                url : "<?php echo base_url();?>main/deleteCart",
                method : "POST",
                data : {row_id : row_id},
                success :function(data){
                  Command: toastr["info"]("Item Dihapus Dari Keranjang Belanja")

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  setTimeout(function () { 
                    location.reload(true); 
                  }, 2000); 
                }
            });
        });
</script>
</body>

</html>