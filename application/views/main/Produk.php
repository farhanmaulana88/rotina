   <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Produk</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url().'main';?>">Home</a>
                        <span>Produk</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
   <!-- Shop Section Begin -->
    <section class="shop spad">
        <div class="container">
            <div class="shop__option">
                <div class="row">
                    <div class="col-lg-9 col-md-9">
                        <div class="shop__option__search">
                            <form action="<?php echo base_url('main/produk');?>" method="post">
                                <select name="id_kategori">
                                    <option value="">- Kategori -</option>
                                    <?php foreach ($kategori as $kategori) { ?>
                                    <option value="<?= $kategori->id;?>" <?= ($kategori->id == $id_kategori) ? 'selected' : '';?>><?= $kategori->kategori;?></option>
                                    <?php } ?>
                                </select>

                                <input type="text" name="pencarian" placeholder="Pencarian" value="<?=$pencarian;?>">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($produk as $produk) { ?>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="product__item">
                            <div class="product__item__pic set-bg" data-setbg="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_utama);?>">
                                <div class="product__label">
                                    <span><?= $produk->kategori;?></span>
                                </div>
                            </div>
                            <div class="product__item__text">
                                <h6><a  href="<?php echo base_url('main/produkdetail/'.$produk->id.'/'.$produk->id_kategori);?>"><?= $produk->nama_produk;?></a></h6>
                                <div class="product__item__price">Rp. <?= number_format($produk->harga_min,0,',','.');?> - Rp. <?= number_format($produk->harga_max,0,',','.');?></div>
                                <div class="cart_add">
                                    <a class="input_cart more" id="<?= $produk->id;?>">Tambah Keranjang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                
            </div>
            <div class="shop__last__option">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <?php if ($id_kategori != '' or $pencarian != '') { ?>
                        <?php }else{ ?>
                        <?php echo $pagination; ?>
                        <?php } ?> 
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="shop__last__text">
                            <p>Menampilkan <?= $didapat;?> <?= $keseluruhan;?> produk</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Shop Section End -->