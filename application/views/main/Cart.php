    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Shopping cart</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="shopping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th>No. </th>
                                    <th>Product / Harga</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <form id="form_cart" action="<?= base_url('main/updateCart');?>" method="post">
                            <tbody>
                                <?php $no=0; foreach ($this->cart->contents() as $items) { $no++; ?>
                                    <tr>
                                    <input type="hidden" name="cart[<?php echo $items['id'];?>][id]" value="<?php echo $items['id'];?>" />
                                    <input type="hidden" name="cart[<?php echo $items['id'];?>][rowid]" value="<?php echo $items['rowid'];?>" />
                                    <input type="hidden" name="cart[<?php echo $items['id'];?>][name]" value="<?php echo $items['name'];?>" />
                                    <input type="hidden" name="cart[<?php echo $items['id'];?>][price]" value="<?php echo $items['price'];?>" />
                                    <input type="hidden" name="cart[<?php echo $items['id'];?>][gambar]" value="<?php echo $items['gambar'];?>" />
                                    <input type="hidden" name="cart[<?php echo $items['id'];?>][qty]" value="<?php echo $items['qty'];?>" />
                                        <td class="cart__price"><?= $no;?>.</td>
                                        <td class="product__cart__item">
                                            <div class="product__cart__item__pic">
                                                <img style="width: 150px; height: 150px" src="<?php echo base_url('assets/GambarProduk/'.$items['gambar']);?>" alt="">
                                            </div>
                                            <div class="product__cart__item__text">
                                                <h6><?= $items['name'];?></h6>
                                                <h5>Rp. <?= number_format($items['price']);?></h5>
                                            </div>
                                        </td>
                                        <td class="quantity__item">
                                            <div class="quantity">
                                                <div class="pro-qty">
                                                    <input type="text" onkeypress="return isNumberKey(event)" name="cart[<?php echo $items['id'];?>][qty]" value="<?= $items['qty'];?>">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="cart__price">Rp. <?= number_format($items['subtotal']);?></td>
                                        <td class="cart__close"><span id="<?= $items['rowid'];?>" class="icon_close deleteCart"></span></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="continue__btn">
                                <a href="<?php echo base_url('main/produk');?>">Continue Shopping</a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="continue__btn update__btn">
                                <a class="more" href="javascript:{}" onclick="document.getElementById('form_cart').submit();"><i class="fa fa-spinner"></i> Update cart</a>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
                <div class="col-lg-4">
                    <div class="cart__discount">
                        <h6>Kode Diskon</h6>
                        <form action="<?php echo base_url('main/formCart');?>" method="post">
                            <input type="text" name="voucher" placeholder="Kode Voucher" value="<?=$voucher;?>">
                            <button type="submit">Apply</button>
                        </form>
                        <br>
                        <?= $pesan;?>
                    </div>
                    <div class="cart__total">
                        <h6>Cart total</h6>
                        <ul>
                            <li>Subtotal <span>Rp. <?=number_format($this->cart->total());?></span></li>
                            <li>Potongan <span>Rp. <?=$potongan;?></span></li>
                            <li>Total <span>Rp. <?=number_format($hasil_potongan);?></span></li>
                        <?php if ($this->session->userdata('status') != 'login') { ?>
                            </ul>
                            <a href="#" data-toggle="modal" data-target="#loginModal" class="modalLogin primary-btn">Proceed to checkout</a>'
                        <?php }else{ ?>
                            <form id="form_checkout" action="<?php echo base_url('main/order');?>" method="post"> 
                                    <li>Cara Pembayaran</li>
                                </ul>
                                <!-- <ul>
                                    <li>
                                        <input type="radio" name="metode_pembayaran" class="form-control" style="width: 30px" value='Langsung' checked>Langsung
                                    </li>
                                    <li>
                                        <input type="radio" name="metode_pembayaran" class="form-control" style="width: 30px" value='Transfer'>Transfer
                                        
                                    </li>
                                </ul> -->
                                <center>
                                     <div class="row">
                                        <div>
                                            <input type="radio" name="metode_pembayaran" class="form-control" style="width: 30px" value='Langsung' checked>Langsung
                                        </div>
                                        &emsp;
                                        <div>
                                            <input type="radio" name="metode_pembayaran" class="form-control" style="width: 30px" value='Transfer'>Transfer
                                        </div>
                                    </div>    
                                </center>
                                <br>
                                <input type="hidden" name="hasil_potongan" value="<?=$hasil_potongan;?>">
                                <a href="javascript:{}" onclick="document.getElementById('form_checkout').submit();" class="more primary-btn">Proceed to checkout</a>'
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shopping Cart Section End -->

    <script type="text/javascript">
        function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if ((charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    </script>

