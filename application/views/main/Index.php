
    <!-- Hero Section Begin -->
    <section class="hero">
        <div class="hero__slider owl-carousel">
            <?php foreach ($promo_thumbnail as $promo) { ?>
                <?php if ($promo->id_ketersediaan != 2) { ?>
                    <div class="hero__item set-bg" data-setbg="<?php echo base_url('assets/GambarPromo/'.$promo->gambar);?>">
                        <div class="container">
                            <div class="row d-flex justify-content-center">
                                <div class="col-lg-8">
                                    <div class="hero__text">
                                        <h2><?= $promo->nama_promo;?></h2>
                                        <a href="<?php echo base_url('main/promoDetail/'.$promo->id);?>" class="primary-btn">Promo</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
            <?php } ?>
        </div>
    </section>
    <!-- Hero Section End -->
    <br>

    <!-- Class Section Begin -->
    <section class="class spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <div class="section-title">
                        <span>Produk Terbaru</span>
                        <h2>Rotina </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($produk as $produk) { ?>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_utama);?>">
                            <div class="product__label">
                                <span><?= $produk->kategori;?></span>
                            </div>
                        </div>
                        <div class="product__item__text">
                            <h6><a href="<?php echo base_url('main/produkdetail/'.$produk->id.'/'.$produk->id_kategori);?>"><?= $produk->nama_produk;?></a></h6>
                            <div class="product__item__price">Rp. <?= number_format($produk->harga_min,0,',','.');?> - Rp. <?= number_format($produk->harga_max,0,',','.');?></div>
                            <div class="cart_add">
                                <?php if ($produk->stok < 1) { ?>
                                    <a style="color: red">Stok Habis</a>
                                <?php }else{ ?>
                                    <a class="input_cart more" id="<?= $produk->id;?>" disabled>Tambah Keranjang</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>   
            </div>
        </div>
    </section>
    <!-- Class Section End -->

    <!-- Testimonial Section Begin -->
    <section class="instagram spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-title">
                        <span>Tim Kami</span>
                        <h2>Rotina</h2>
                    </div>
                </div>
            </div>
                    <center>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                </div>
                
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="team__item set-bg" data-setbg="<?php echo base_url('assets/Team/'.$leader->foto);?>">
                        <div class="team__item__text">
                            <h6><?=$leader->nama;?></h6>
                            <span><?=$leader->jabatan;?></span>
                            <div class="team__item__social">
                                <a href="https://www.facebook.com/<?=$leader->facebook;?>"><i class="fa fa-facebook"></i></a>
                                <a href="https://www.twitter.com/<?=$leader->twitter;?>"><i class="fa fa-twitter"></i></a>
                                <a href="https://www.instagram.com/<?=$leader->instagram;?>"><i class="fa fa-instagram"></i></a>
                                <a href="https://www.youtube.com/<?=$leader->youtube;?>"><i class="fa fa-youtube-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="col-lg-4 col-md-6 col-sm-6">
                </div> -->
                
            </div>
                </center>
            <div class="row">
                <?php foreach ($team as $team) { ?>
                    <div class="col-lg-2 col-md-4 col-sm-4">
                        <div class="team__item set-bg" data-setbg="<?php echo base_url('assets/Team/'.$team->foto);?>">
                            <div class="team__item__text">
                                <h6><?= $team->nama;?></h6>
                                <span><?= $team->jabatan;?></span>
                                <div class="team__item__social">
                                    <a href="https://www.facebook.com/<?= $team->facebook;?>"><i class="fa fa-facebook"></i></a>
                                    <a href="https://www.twitter.com/<?= $team->twitter;?>"><i class="fa fa-twitter"></i></a>
                                    <a href="https://www.instagram.com/<?= $team->instagram;?>"><i class="fa fa-instagram"></i></a>
                                    <a href="https://www.youtube.com/<?= $team->youtube;?>"><i class="fa fa-youtube-play"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Instagram Section Begin -->
    <section class="testimonial spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 p-0">
                    <div class="instagram__text">
                        <div class="section-title">
                            <span>Follow us on instagram</span>
                            <h2>Sweet moments are saved as memories.</h2>
                        </div>
                        <h5><i class="fa fa-instagram"></i> <?= $instagram->instagram;?></h5>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                            <div class="instagram__pic">
                                <img src="<?php echo base_url('assets/instagram/'.$instagram->gambar1);?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                            <div class="instagram__pic middle__pic">
                                <img src="<?php echo base_url('assets/instagram/'.$instagram->gambar2);?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                            <div class="instagram__pic">
                                <img src="<?php echo base_url('assets/instagram/'.$instagram->gambar3);?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                            <div class="instagram__pic">
                                <img src="<?php echo base_url('assets/instagram/'.$instagram->gambar4);?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                            <div class="instagram__pic middle__pic">
                                <img src="<?php echo base_url('assets/instagram/'.$instagram->gambar5);?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                            <div class="instagram__pic">
                                <img src="<?php echo base_url('assets/instagram/'.$instagram->gambar6);?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Instagram Section End -->

    <!-- Map Begin -->
    <div class="map">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-7">
                </div>
            </div>
        </div>
        <div class="map__iframe">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.8059473798053!2d108.07181891477157!3d-6.670951295176654!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6ed498e6158cd3%3A0xa83e200645ef9248!2sTomo%20Food%20Industri!5e0!3m2!1sid!2sid!4v1608780835322!5m2!1sid!2sid" height="300" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
    <!-- Map End -->

