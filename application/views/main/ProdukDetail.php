    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Product detail</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url().'main';?>">Home</a>
                        <a href="<?php echo base_url().'main/produk';?>">Produk</a>
                <?php foreach ($produk as $produk) { ?>
                        <span><?= $produk->nama_produk;?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shop Details Section Begin -->
    <section class="product-details spad">
        <div class="container">
            <div class="row">
                    <div class="col-lg-6">
                        <div class="product__details__img">
                            <div class="product__details__big__img">
                                <img class="big_img" src="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_utama);?>" alt="">
                            </div>
                            <div class="product__details__thumb">
                                <div class="pt__item active">
                                    <img data-imgbigurl="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_utama);?>"
                                    src="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_utama);?>" alt="">
                                </div>
                            

                            <?php if ($produk->gambar_detail1 != '-') { ?>
                                <div class="pt__item active">
                                    <img data-imgbigurl="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail1);?>"
                                    src="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail1);?>" alt="">
                                </div>
                            <?php }else{ ?>

                            <?php } ?>

                            <?php if ($produk->gambar_detail2 != '-') { ?>
                                <div class="pt__item active">
                                    <img data-imgbigurl="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail2);?>"
                                    src="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail2);?>" alt="">
                                </div>
                            <?php }else{ ?>

                            <?php } ?>

                            <?php if ($produk->gambar_detail3 != '-') { ?>
                                <div class="pt__item active">
                                    <img data-imgbigurl="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail3);?>"
                                    src="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail3);?>" alt="">
                                </div>
                            <?php }else{ ?>

                            <?php } ?>

                            <?php if ($produk->gambar_detail4 != '-') { ?>
                                <div class="pt__item active">
                                    <img data-imgbigurl="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail4);?>"
                                    src="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail4);?>" alt="">
                                </div>
                            <?php }else{ ?>

                            <?php } ?>

                            <?php if ($produk->gambar_detail5 != '-') { ?>
                                <div class="pt__item active">
                                    <img data-imgbigurl="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail5);?>"
                                    src="<?php echo base_url('assets/GambarProduk/'.$produk->gambar_detail5);?>" alt="">
                                </div>
                            <?php }else{ ?>

                            <?php } ?>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="product__details__text">
                            <div class="product__label"><?= $produk->kategori;?></div>
                            <h4><?= $produk->nama_produk;?></h4>
                            <h5>Rp. <?= number_format($produk->harga_min,0,',','.');?> - Rp. <?= number_format($produk->harga_max,0,',','.');?></h5>
                            <p><?= $produk->karakteristik;?></p>
                            <ul>
                                <li><span>Komposisi: </span> <?= $produk->komposisi;?></li>
                                <li><span>Gramasi: </span> <?= $produk->gramasi;?> gr/bks</li>
                                <li><span>Ukuran: </span> <?= $produk->ukuran;?></li>
                                <li><span>Rasa: </span> <?= $produk->rasa;?></li>
                                <li><span>Expired: </span> <?= $produk->expired;?></li>
                                <li><span>Stok: </span> <?= $produk->stok;?></li>
                            </ul>
                            <div class="product__details__option">
                                <?php if ($produk->stok > 0 ) { ?>
                                <a href="#" class="input_cart primary-btn" id="<?= $produk->id;?>">Tambah Keranjang</a>
                                <?php }else{ ?>
                                <a href="#" class="primary-btn" style="background-color: red">Habis</a>                                
                                <?php } ?> 
                            </div>

                        </div>
                    </div>
                <?php } ?>
                
            </div>
            <div class="product__details__tab">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" role="tablist">
                        
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Shop Details Section End -->

    <!-- Related Products Section Begin -->
    <section class="related-products spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-title">
                        <h2>Related Products</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="related__products__slider owl-carousel">
                    <?php foreach ($produk_terkait as $produk_terkait) { ?>
                        <div class="col-lg-3">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="<?php echo base_url('assets/GambarProduk/'.$produk_terkait->gambar_utama);?>">
                                    <div class="product__label">
                                        <span><?= $produk_terkait->kategori;?></span>
                                    </div>
                                </div>
                                <div class="product__item__text">
                                    <h6><a  href="<?php echo base_url('main/produkdetail/'.$produk_terkait->id.'/'.$produk_terkait->id_kategori);?>"><?= $produk_terkait->nama_produk;?></a></h6>
                                    <div class="product__item__price">Rp. <?= number_format($produk_terkait->harga_min,0,',','.');?> - Rp. <?= number_format($produk_terkait->harga_max,0,',','.');?></div>
                                    <div class="cart_add">
                                        <a class="input_cart more" id="<?= $produk_terkait->id;?>">Tambah Keranjang</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Related Products Section End -->