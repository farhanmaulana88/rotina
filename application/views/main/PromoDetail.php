    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Promo Detail</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url().'main';?>">Home</a>
                        <a href="<?php echo base_url().'main/promo';?>">Promo</a>
                        <span>Promo Detail</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Blog Section Begin -->
    <section class="blog spad">
        <div class="container">
            <div class="row">
                <?php foreach ($promo as $promo) { ?>
                <div class="col-lg-8">
                    <div class="blog__item">
                        <div class="blog__item__pic set-bg" data-setbg="<?php echo base_url('assets/GambarPromo/'.$promo->gambar);?>">
                            <div class="blog__pic__inner">
                                <div class="label">Promo</div>
                                <ul>
                                    <li>By <span>Rotina</span></li>
                                    <li><?= date('d-M-Y', strtotime($promo->tanggal_mulai_promo));?> - <?= date('d-M-Y', strtotime($promo->tanggal_akhir_promo));?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="blog__item__text">
                            <h2><?= $promo->nama_promo ;?></h2>
                            <p style="text-align: justify;"><?= $promo->isi_promo ;?></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="col-lg-4">
                    <div class="blog__sidebar">
                        <div class="blog__sidebar__item">
                            <h5>Follow Us</h5>
                            <div class="blog__sidebar__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="blog__sidebar__item">
                            <h5>Promo Lainnya</h5>

                            <div class="blog__sidebar__recent">
                                <?php foreach ($promo_thumbnail as $thumbnail) { ?>
                                    <a href="<?php echo base_url('main/promodetail/'.$thumbnail->id);?>" class="blog__sidebar__recent__item">
                                        <div class="blog__sidebar__recent__item__pic">
                                            <img style="width: 100px; height: 100px" src="<?php echo base_url('assets/GambarPromo/'.$thumbnail->gambar);?>" alt="">
                                        </div>
                                        <div class="blog__sidebar__recent__item__text">
                                            <h4><?= $thumbnail->nama_promo ;?></h4>
                                            <span><?= date('d-M-Y', strtotime($thumbnail->tanggal_mulai_promo));?></span>
                                        </div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->