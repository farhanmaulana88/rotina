 <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Promo</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url().'main';?>">Home</a>
                        <span>Promo</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Class Section Begin -->
    <section class="class-page spad">
        <div class="container">
            <div class="shop__option">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="shop__option__search">
                            <form action="<?php echo base_url('main/promo');?>" method="post">
                                <input type="text" name="pencarian" placeholder="Pencarian" value="<?=$pencarian;?>">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <?php foreach ($promo as $promo) { ?>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="class__item">
                                <div class="class__item__pic set-bg" data-setbg="<?php echo base_url('assets/GambarPromo/'.$promo->gambar);?>">
                                    <?php if ($promo->id_ketersediaan == 2) { ?>
                                        <div class="label" style="background-color: red;">Habis</div>
                                    <?php }else{ ?>
                                        <div class="label">Tersedia</div>
                                    <?php } ?>
                                </div>
                                <div class="class__item__text">
                                    <h5><a href="<?php echo base_url('main/promodetail/'.$promo->id);?>"><?= $promo->nama_promo ;?></a></h5>
                                    <span><?= date('d-M-Y', strtotime($promo->tanggal_mulai_promo));?> - <?= date('d-M-Y', strtotime($promo->tanggal_akhir_promo));?></span>
                                   <!--  <p><?= $promo->isi_promo ;?></p> -->
                                    <a href="<?php echo base_url('main/promodetail/'.$promo->id);?>" class="read_more">Read more</a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="shop__last__option">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <?php if ($pencarian != '') { ?>
                                <?php }else{ ?>
                                <?php echo $pagination; ?>
                                <?php } ?> 
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="shop__last__text">
                                    <p>Menampilkan <?= $didapat;?> <?= $keseluruhan;?> produk</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Class Section End -->