    <!-- Contact Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="map">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-4 col-md-7">
                        </div>
                    </div>
                </div>
                <div class="map__iframe">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.8059473798053!2d108.07181891477157!3d-6.670951295176654!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6ed498e6158cd3%3A0xa83e200645ef9248!2sTomo%20Food%20Industri!5e0!3m2!1sid!2sid!4v1608780835322!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
            </div>
            <div class="contact__address">
                <div class="row">
                    <?php foreach ($lokasi as $key) { ?>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="contact__address__item">
                                <h6><?= $key->kota;?></h6>
                                <ul>
                                    <li>
                                        <span class="icon_pin_alt"></span>
                                        <p><?= $key->alamat;?></p>
                                    </li>
                                    <li><span class="icon_headphones"></span>
                                        <p><?= $key->no_telp;?></p>
                                    </li>
                                    <li><span class="icon_mail_alt"></span>
                                        <p><?= $key->email;?></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Section End -->