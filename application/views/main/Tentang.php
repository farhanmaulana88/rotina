 <!-- Blog Hero Begin -->
    <div class="blog-hero set-bg" data-setbg="<?php echo base_url();?>assets/cake-main/img/blog/details/blog-hero.jpg">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-5">
                    <div class="blog__hero__text">
                        <div class="label">Tentang</div>
                        <h2>Rotina</h2>
                        <ul>
                            <li>By <span>PT Tomo Food Industri</span></li>
                            <li>2011</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Blog Hero End -->

    <!-- Blog Details Section Begin -->
    <section class="blog-details spad">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="blog__details__content">
                        <div class="blog__details__share">
                            <a href="http://facebook.com/<?= $social_media->facebook;?>" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="http://twitter.com/<?= $social_media->twitter;?>" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="http://instagram.com/<?= $social_media->instagram;?>" class="instagram"><i class="fa fa-instagram"></i></a>
                        </div>
                        <div class="blog__details__text">
                            <?= $tentang->deskripsi;?>
                        </div>
                        <div class="blog__details__recipe">
                            <div class="blog__details__recipe__item">
                                <h6><img src="<?php echo base_url();?>assets/cake-main/img/blog/details/user.png" alt=""> PETINGGI</h6>
                                <span><?= $tentang->petinggi;?> Petinggi</span>
                            </div>
                            <div class="blog__details__recipe__item">
                                <h6><img src="<?php echo base_url();?>assets/cake-main/img/blog/details/clock.png" alt=""> PEKERJA</h6>
                                <span><?= $tentang->pekerja;?> Karyawan</span>
                            </div>
                            <div class="blog__details__recipe__item">
                                <h6><img src="<?php echo base_url();?>assets/cake-main/img/blog/details/date.png" alt=""> TANGGAL BERDIRI</h6>
                                <span><?= switch_tgl($tentang->tanggal_berdiri, 'd M Y') ;?></span>
                            </div>
                        </div>
                        <div class="blog__details__recipe__details">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="blog__details__ingredients">
                                        <h6>Visi</h6>
                                        <?= $tentang->visi;?>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="blog__details__ingredients__pic">
                                        <img src="<?php echo base_url();?>assets/cake-main/img/blog/details/blog-details.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog__details__direction">
                            <h6>Misi</h6>
                            <?= $tentang->misi;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Details Section End -->