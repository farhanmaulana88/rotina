 <style type="text/css">
    .more {
      cursor:pointer;
    }
 </style>
   <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Kelola Konten Website</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url('admin/konten');?>">Konten</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
    <!-- =============================================================================================================================== -->
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h4>Team Rotina</h4>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table id="tabeluser" width="100%">
                            <thead>
                                <tr>
                                    <th>Foto / Nama </th>
                                    <th>Facebook </th>
                                    <th>Twitter </th>
                                    <th>Instagram </th>
                                    <th>Youtube </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($team as $team) { ?>
                                <tr>
                                     <td class="product__cart__item">
                                        <div class="product__cart__item__pic">
                                            <img style="width: 140px;height: 180px;" src="<?php echo base_url().'assets/Team/'.$team->foto;?>" alt="">
                                        </div>
                                        <div class="product__cart__item__text">
                                            <h6><?php echo $team->nama;?></h6>
                                        </div>
                                    </td>
                                    <td class="cart__price"><?php echo $team->facebook;?></td>
                                    <td class="cart__price"><?php echo $team->twitter;?></td>
                                    <td class="cart__price"><?php echo $team->instagram;?></td>
                                    <td class="cart__price"><?php echo $team->youtube;?></td>
                                    <td class="cart__close">
                                        <button class="btn btn-info btn-sm view_data_team" id="<?php echo $team->id; ?>"><i class="fa fa-pencil"></i></button>
                                        <a href="<?php echo base_url('admin/deleteTeam/'.$team->id);?>" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                                        
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="continue__btn update__btn">
                                <a href="#" data-toggle="modal" data-target="#createTeamModal"><i class="fa fa-spinner"></i> Tambah Team </a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>

    <!-- =============================================================================================================================== -->    
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                            <?php foreach ($instagram as $instagram) { ?>
                        <h4>Instagram <?= $instagram->instagram;?></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table >
                            <thead>
                                <tr>
                                    <th width="33%"></th>
                                    <th width="33%"><center>Gambar</center></th>
                                    <th width="33%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="product__cart__item">
                                        <center>
                                            <img style="width: 200px;height: 200px;position: center" src="<?php echo base_url().'assets/instagram/'.$instagram->gambar1;?>" alt="">
                                        </center>
                                    </td>
                                    <td class="cart__price">
                                        <center>
                                            <img style="width: 200px;height: 200px;position: center" src="<?php echo base_url().'assets/instagram/'.$instagram->gambar2;?>" alt="">
                                        </center>
                                    </td>
                                    <td class="product__cart__item">
                                        <center>
                                            <img style="width: 200px;height: 200px;position: center" src="<?php echo base_url().'assets/instagram/'.$instagram->gambar3;?>" alt="">
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="product__cart__item">
                                        <center>
                                            <img style="width: 200px;height: 200px;position: center" src="<?php echo base_url().'assets/instagram/'.$instagram->gambar4;?>" alt="">
                                        </center>
                                    </td>
                                    <td class="product__cart__item">
                                        <center>
                                            <img style="width: 200px;height: 200px;position: center" src="<?php echo base_url().'assets/instagram/'.$instagram->gambar5;?>" alt="">
                                        </center>
                                    </td>
                                    <td class="product__cart__item">
                                        <center>
                                            <img style="width: 200px;height: 200px;position: center" src="<?php echo base_url().'assets/instagram/'.$instagram->gambar6;?>" alt="">
                                        </center>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="continue__btn update__btn">
                                <a id="1" class="more view_data_instagram"><i class="fa fa-pencil"></i> Update Konten Instagram </a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>

    <!-- =============================================================================================================================== -->
    <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                       <h4>Tentang Rotina</h4> 
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table >
                            <thead>
                                <tr>
                                    <th width="10%">Data</th>
                                    <th width="90%"><center>Isi</center></th>
                                </tr>
                            </thead>
                            <form id="form_tentang" method="post" action="<?php echo base_url('admin/updateTentang');?>">
                            <tbody>
                                <tr>
                                    <td><b>Deskripsi</b></td>
                                    <td>           
                                        <textarea id="summernote" name="deskripsi" ><?= $tentang->deskripsi;?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Visi</b></td>
                                    <td>           
                                        <textarea id="summernote2" name="visi" ><?= $tentang->visi;?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Misi</b></td>
                                    <td>           
                                        <textarea id="summernote3" name="misi" ><?= $tentang->misi;?></textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <thead>
                                <tr>
                                    <th width="33%"><center>Petinggi</center></th>
                                    <th width="33%"><center>Jumlah Pekerja</center></th>
                                    <th width="33%"><center>Tanggal Berdiri</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><center><input class="form-control" type="text" name="petinggi" value="<?= $tentang->petinggi;?>"></center></td>
                                    <td><center><input class="form-control" type="text" name="pekerja" value="<?= $tentang->pekerja;?>"></center></td>
                                    <td><center><input class="form-control" type="date" name="tanggal_berdiri" value="<?= $tentang->tanggal_berdiri;?>"></center></td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <thead>
                                <tr>
                                    <th width="33%"><center>Facebook</center></th>
                                    <th width="33%"><center>Twitter</center></th>
                                    <th width="33%"><center>Instagram</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><center><input class="form-control" type="text" name="facebook" value="<?= $social_media->facebook;?>"></center></td>
                                    <td><center><input class="form-control" type="text" name="twitter" value="<?= $social_media->twitter;?>"></center></td>
                                    <td><center><input class="form-control" type="text" name="instagram" value="<?= $social_media->instagram;?>"></center></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="continue__btn update__btn">
                                <a href="javascript:{}" onclick="document.getElementById('form_tentang').submit();" class="more view_data_instagram"><i class="fa fa-pencil"></i> Update Konten Tentang </a> 
                            </div>
                        </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <!-- ====================================================================================================================== -->
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h4>Lokasi Pusat/Cabang Rotina</h4>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table id="tabeluser2" width="100%">
                            <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="15%%">Kota </th>
                                    <th width="35%">Alamat </th>
                                    <th width="15%">No. Telpon </th>
                                    <th width="20%">Email </th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no=0; foreach ($lokasi as $lokasi) { $no++; ?>
                                <tr>
                                    <td class="cart__price"><?=$no;?></td>
                                    <td class="cart__price"><?=$lokasi->kota;?></td>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__text">
                                            <h6><?=$lokasi->alamat;?></h6>
                                        </div>
                                    </td>
                                    <td class="cart__price"><?=$lokasi->no_telp;?></td>
                                    <td class="cart__price"><?=$lokasi->email;?></td>
                                    <td class="cart__close">
                                        <button class="btn btn-info btn-sm view_data_lokasi" id="<?=$lokasi->id; ?>"><i class="fa fa-pencil"></i></button>
                                        <a href="<?php echo base_url('admin/deleteLokasi/'.$lokasi->id);?>" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                                        
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="continue__btn update__btn">
                                <a href="#" data-toggle="modal" data-target="#createLokasiModal"><i class="fa fa-spinner"></i> Tambah Lokasi </a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>

    <!-- =============================================================================================================================== -->
    </section>
    <!-- Shopping Cart Section End -->

    <!-- =============================================================================================================================== -->

     <!-- Update Modal Team -->
    <div class="modal fade" id="teamModal" tabindex="-1" role="dialog" aria-labelledby="teamModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="teamModalLabel"><b>Update Konten Team</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="teamHasil"></div>

        </div>
      </div>
     </div>
    </div>

         <!-- Create Modal Team -->
    <div class="modal fade" id="createTeamModal" tabindex="-1" role="dialog" aria-labelledby="createTeamModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="createTeamModalLabel"><b>Tambah Team</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="checkout__form">
                <form action="<?php echo base_url('admin/createTeam');?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Nama<span>*</span></p>
                                        <input type="text" name="nama">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Jabatan<span>*</span></p>
                                        <input type="text" name="jabatan" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Foto<span>*</span></p>
                                        <input type="file" name="foto" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Facebook</p>
                                        <input type="text" class="form-control" name="facebook">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Twitter</p>
                                        <input type="text" class="form-control" name="twitter">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Instagram</p>
                                        <input type="text" class="form-control" name="instagram">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Youtube</p>
                                        <input type="text" class="form-control" name="youtube">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>

        </div>
      </div>
     </div>
    </div>

    <!-- =============================================================================================================================== -->

    <!-- Update Instagram Team -->
    <div class="modal fade" id="instagramModal" tabindex="-1" role="dialog" aria-labelledby="instagramModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="instagramModalLabel"><b>Update Konten Instagram</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="instagramHasil"></div>

        </div>
      </div>
     </div>
    </div>

     <!-- Update Modal Team -->
    <div class="modal fade" id="teamModal" tabindex="-1" role="dialog" aria-labelledby="teamModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="teamModalLabel"><b>Update Konten Team</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="teamHasil"></div>

        </div>
      </div>
     </div>
    </div>

       <!-- =============================================================================================================================== -->

         <!-- Create Lokasi Team -->
    <div class="modal fade" id="createLokasiModal" tabindex="-1" role="dialog" aria-labelledby="createLokasiModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="createLokasiModalLabel"><b>Tambah Lokasi</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="checkout__form">
                <form action="<?= base_url('admin/createLokasi');?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Nama Kota/Kabupaten<span>*</span></p>
                                        <input type="text" required name="kota">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Alamat<span>*</span></p>
                                        <textarea style="height:200px" class="form-control" required name="alamat"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>No. Telpon <span>*</span></p>
                                        <input type="text" class="form-control" required name="no_telp">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Email <span>*</span></p>
                                        <input type="text" class="form-control" required name="email">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>

        </div>
      </div>
     </div>
    </div>

    <!-- Update Modal Team -->
    <div class="modal fade" id="lokasiModal" tabindex="-1" role="dialog" aria-labelledby="lokasiModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="lokasiModalLabel"><b>Update Konten Lokasi</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="lokasiHasil"></div>

        </div>
      </div>
     </div>
    </div>

    <!-- =============================================================================================================================== -->