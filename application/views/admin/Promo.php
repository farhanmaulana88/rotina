   <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Kelola Produk</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url('admin/kategori');?>">Kategori</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table id="tabeluser" width="100%">
                            <thead>
                                <tr>
                                    <th>Promo </th>
                                    <th><center>Tanggal Promo</center></th>
                                    <th>Ketersediaan Promo </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($promo as $key) { ?>
                                <tr>
                                     <td class="product__cart__item">
                                        <div class="product__cart__item__pic">
                                            <img style="width: 280px;height: 130px;" src="<?php echo base_url().'assets/GambarPromo/'.$key->gambar;?>" alt="">
                                        </div>
                                        <div class="product__cart__item__text">
                                            <h6><?php echo $key->nama_promo;?></h6>
                                        </div>
                                    </td>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__text">
                                            <h6><center><?php echo date('d-M-Y', strtotime($key->tanggal_mulai_promo));?> s/d <?php echo date('d-M-Y', strtotime($key->tanggal_akhir_promo));?></center></h6>
                                        </div>
                                    </td>
                                    <td class="cart__price"><?php echo $key->ketersediaan;?></td>
                                    <td class="cart__close">
                                        <button class="btn btn-info btn-sm view_data_promo" value="Update" id="<?php echo $key->id; ?>"><i class="fa fa-pencil"></i></button>
                                        <a href="<?php echo base_url('admin/deletePromo/'.$key->id);?>" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                                        
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="continue__btn update__btn">
                                <a href="#" data-toggle="modal" data-target="#createPromoModal"><i class="fa fa-spinner"></i> Tambah Promo </a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shopping Cart Section End -->

     <!-- Create Kategori Modal -->
    <div class="modal fade" id="createPromoModal" tabindex="-1" role="dialog" aria-labelledby="promoModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="promoModalLabel"><b>Tambah Promo</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="checkout__form">
                <form action="<?php echo base_url('admin/createPromo');?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Nama Promo<span>*</span></p>
                                        <input type="text" name="nama_promo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Tanggal Mulai<span>*</span></p>
                                        <input type="date" name="tanggal_mulai_promo">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Tanggal Selesai<span>*</span></p>
                                        <input type="date" name="tanggal_akhir_promo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Voucher </p>
                                        <input type="text" name="voucher">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Potongan</p>
                                        <input type="text" name="potongan">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Minimum Belanja</p>
                                        <input type="text" name="min_belanja">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Isi Promo<span>*</span></p>
                                        <textarea id="summernotePromo2" name="isi_promo" ></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Gambar<span>*</span></p>
                                        <input type="file" class="form-control" name="gambar">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>

        </div>
      </div>
     </div>
    </div>

    <!-- Edit Promo Modal -->
    <div class="modal fade" id="promoModal" tabindex="-1" role="dialog" aria-labelledby="promoModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="promoModalLabel"><b>Update promo</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="promoHasil"></div>

        </div>
      </div>
     </div>
    </div>