    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Tambah Produk</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url('admin');?>">Produk</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Checkout Section Begin -->
    <section class="checkout spad">
        <div class="container">
            <div class="checkout__form">
                <form action="<?php echo base_url('admin/createProduk');?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <h6 class="checkout__title">Isikan Detail Produk!</h6>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Nama Produk<span>*</span></p>
                                        <input type="text" name="nama_produk">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Kategori<span>*</span></p>
                                        <select name="id_kategori">
                                            <?php foreach ($kategori as $key) { ?>
                                            <option value="<?php echo $key->id;?>"><?php echo $key->kategori;?></option>
                                            <?php } ?>                                   
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gramasi<span>*</span></p>
                                        <input type="text" name="gramasi">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Ukuran<span>*</span></p>
                                        <input type="text" name="ukuran">
                                    </div>
                                </div>
                            </div>
                            <div class="checkout__input">
                                <p>Komposisi<span>*</span></p>
                                <input type="text" name="komposisi">
                            </div>
                            <div class="checkout__input">
                                <p>Rasa<span>*</span></p>
                                <input type="text" name="rasa">
                            </div>
                            <div class="checkout__input">
                                <p>Karakteristik<span>*</span></p>
                                <input type="text" name="karakteristik">
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Expired<span>*</span></p>
                                        <input type="text" name="expired">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Stok<span>*</span></p>
                                        <input type="text" name="stok">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Harga Minimal<span>*</span></p>
                                        <input id="min" type="text" name="harga_min">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Harga Maksimal<span>*</span></p>
                                        <input id="max" type="text" name="harga_max">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Utama<span>*</span></p>
                                        <input type="file" name="gambar_utama">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 1<span>*</span></p>
                                        <input type="file" name="gambar_detail1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 2<span>*</span></p>
                                        <input type="file" name="gambar_detail2">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 3<span>*</span></p>
                                        <input type="file" name="gambar_detail3">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 4<span>*</span></p>
                                        <input type="file" name="gambar_detail4">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 5<span>*</span></p>
                                        <input type="file" name="gambar_detail5">
                                    </div>
                                </div>
                            </div>
                           <button type="submit" class="site-btn">SIMPAN</button>
                           
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- Checkout Section End -->