   <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Kelola Produk</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url('admin');?>">Produk</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table id="tabeluser">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Komposisi</th>
                                    <th>Harga</th>
                                    <th>Kategori</th>
                                    <th>Stok</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($produk as $key) { ?>
                                <tr>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__pic">
                                            <img style="width: 120px;height: 120px;" src="<?php echo base_url().'assets/GambarProduk/'.$key->gambar_utama;?>" alt="">
                                        </div>
                                        <div class="product__cart__item__text">
                                            <h6><?php echo $key->nama_produk;?></h6>
                                            <h5>Rp <?=number_format($key->harga_min, 0, ".", ".");?> - Rp <?=number_format($key->harga_max, 0, ".", ".");?></h5>
                                        </div>
                                    </td>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__text">
                                            <h6><?php echo $key->komposisi;?></h6>
                                        </div>
                                    </td>
                                    <td class="cart__price"><?php echo $key->gramasi;?> Gr/Bks</td>
                                    <td class="cart__price"><?php echo $key->kategori;?></td>
                                    <td class="cart__price"><?php echo $key->stok;?> Pcs</td>
                                    <td class="cart__close">
                                        <button class="btn btn-info btn-sm view_data" id="<?php echo $key->id; ?>"><i class="fa fa-pencil"></i></button>
                                        <a href="<?php echo base_url('admin/deleteProduk/'.$key->id);?>" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>   
                                        
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="continue__btn update__btn">
                                <a href="<?php echo base_url('admin/formCreateProduk');?>"><i class="fa fa-spinner"></i> Tambah Produk </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shopping Cart Section End -->

     <!-- Update Modal -->
    <div class="modal fade" id="produkModal" tabindex="-1" role="dialog" aria-labelledby="produkModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="produkModalLabel"><b>Update Produk</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="produkHasil"></div>

        </div>
      </div>
     </div>
    </div>