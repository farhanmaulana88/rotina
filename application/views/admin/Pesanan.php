
    <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/jquery.fancybox.css?v=2.1.5" media="screen" type="text/css">
    <script src="<?php echo base_url();?>assets/cake-main/js/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript">
        $(document).ready(function(){
         // Initiate DataTable function comes with plugin
         $('.perbesar').fancybox();
     });
    </script>
  <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Kelola Produk</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url('admin/kategori');?>">Kategori</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table id="tabeluser" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">No. </th>
                                    <th width="15%">No. Pesanan </th>
                                    <th width="15%">Pembeli </th>
                                    <th width="15%">Total </th>
                                    <th width="15%">Pembayaran </th>
                                    <th width="15%">Status </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $no = 0;
                            foreach ($pesanan as $key) { 
                                if ($key->bukti != '') {
                                        $bukti = $key->bukti;
                                    }else{
                                        $bukti = 'no-photo.jpg';
                                    }
                            $no++;
                            ?>
                                <tr>
                                    <td class="cart__price"><?php echo $no;?></td>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__text">
                                            <h6><?php echo $key->no_pesanan;?></h6>
                                        </div>
                                    </td>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__text">
                                            <h6><?php echo $key->nama;?></h6>
                                        </div>
                                    </td>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__text">
                                            <h6>Rp. <?php echo number_format($key->total);?></h6>
                                        </div>
                                    </td>
                                    <?php if ($key->metode_pembayaran == "Langsung") { ?>
                                        <td class="product__cart__item">
                                            <div class="product__cart__item__text">
                                                <h6><?php echo $key->metode_pembayaran;?></h6>
                                            </div>
                                        </td>
                                    <?php }else{ ?>
                                        <td class="product__cart__item">
                                            <div class="product__cart__item__text">
                                                <a href="<?php echo base_url('assets/BuktiPembayaran/'.$bukti);?>" class="perbesar">
                                                    <img id="myImg" src="<?php echo base_url('assets/BuktiPembayaran/'.$bukti);?>">
                                                </a>
                                            </div>
                                        </td>
                                    <?php } ?>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__text">
                                            <h6><?php echo $key->status_pesanan;?></h6>
                                        </div>
                                    </td>
                                    <td class="cart__close">
                                        <button class="btn btn-info btn-sm view_data_pembayaran" id="<?php echo $key->id; ?>"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-success btn-sm view_data_pembelian" id="<?php echo $key->id; ?>"><i class="fa fa-search"></i></button>                                      
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shopping Cart Section End -->

    <!-- Contoh Modal -->
    <div class="modal fade" id="pembayaranModal" tabindex="-1" role="dialog" aria-labelledby="pembayaranModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="pembayaranModalLabel"><b>Detail Pembayaran</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="pembayaranHasil"></div>

        </div>
      </div>
     </div>
    </div>

    <!-- Contoh Modal -->
    <div class="modal fade" id="pembelianModal" tabindex="-1" role="dialog" aria-labelledby="pembelianModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="pembelianModalLabel"><b>Detail Pembelian Roti</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="pembelianHasil"></div>

        </div>
      </div>
     </div>
    </div>