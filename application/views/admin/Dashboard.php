   <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Dashboard Grafik</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url().'main/dashboard';?>">Home</a>
                        <span>Dashboard</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <section class="shopping-cart spad">
        <div class="container">
        	<form action="<?php echo base_url('admin/dashboard');?>" method="post">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
						<div class="input-group-append">
								<input type="text" class="form-control datepickYear" value="<?= $tahun;?>" data-date-format="yyyy" name="tahun" id="datepicker" readonly="true">
								<div class="input-group-text"><i class="fa fa-calendar"></i></div>
						</div>
                    </div>
                    <div class="col-lg-1 col-md-1">
						<div class="input-group-append">
								<!-- <div class="input-group-text"><input type="submit"><i class="fa fa-calendar"></i></button></div> -->
								<button class="form-control datepickYear" type="submit"><i class="fa fa-search"></i></button>
						</div>
                    </div>
                </div>
            </form>
        	<div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
						<div>
							<canvas id="myChart4"></canvas>
						</div>
					</div>
				</div>
			</div>
			<br>
			<br>
            <div class="row">
                <div class="col-lg-6">
                    <div class="shopping__cart__table">
						<div>
							<canvas id="myChart"></canvas>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="shopping__cart__table">
						<div>
							<canvas id="myChart2"></canvas>
						</div>
					</div>
				</div>
			</div>
			<br>
			<br>
			<div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                    	<div>
							<canvas id="myChart3"></canvas>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
    //Inisialisasi nilai variabel awal
    $bulan= "";
    $jumlah=null;
    $pendapatan=null;
    foreach ($penjualan as $key)
    {
        $bul=$key->bulan;
        $bulan .= "'$bul'". ", ";
        $jum=$key->penjualan;
        $jumlah .= "$jum". ", ";
        $pen= $key->pendapatan;
        $pendapatan .= "$pen". ", ";
    }

    $nama_produk= "";
    $jml_produk=null;
    foreach ($produk as $key2)
    {
        $pro=$key2->nama_produk;
        $nama_produk .= "'$pro'". ", ";
        $jml=$key2->total;
        $jml_produk .= "$jml". ", ";
    }


    ?>

<script type="text/javascript">
	


	var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [<?php echo $bulan; ?>],
                datasets: [{
                    label: 'Grafik Penjualan Perbulan '+<?=$tahun;?>,
                    data: [<?php echo $jumlah; ?>],
                    borderColor: [
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		            ],
                    backgroundColor: [
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		            ],
                    borderWidth: 3
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });

        var ctx2 = document.getElementById("myChart2").getContext('2d');
        var myChart = new Chart(ctx2, {
            type: 'line',
            data: {
                labels: [<?php echo $bulan; ?>],
                datasets: [{
                    label: 'Grafik Pendapatan Perbulan '+<?=$tahun;?>,
                    data: [<?php echo $pendapatan; ?>],
                    borderColor: [
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		            ],
                    backgroundColor: [
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		            ],
                    borderWidth: 3
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });

        var ctx3 = document.getElementById("myChart3").getContext('2d');
        var chartOptions = {
		  scales: {
		    yAxes: [{
		      barPercentage: 0.5
		    }]
		  },
		  elements: {
		    rectangle: {
		      borderSkipped: 'left',
		    }
		  }
		}
        var myChart = new Chart(ctx3, {
            type: 'horizontalBar',
            data: {
                labels: [<?php echo $nama_produk; ?>],
                datasets: [{
                    label: 'Grafik Penjualan Kue Rotina '+<?=$tahun;?>,
                    data: [<?php echo $jml_produk; ?>],
                    borderColor: [
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		                'rgba(255, 159, 64, 1)',
		            ],
                    backgroundColor: [
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		                'grey',
		            ],
                    borderWidth: 3
                }]
            },
            options: chartOptions
        });

        var ctx4 = document.getElementById("myChart4").getContext('2d');
        var data = {
		    labels: [
		        "Pengunjung Total",
		        "Hari Ini",
		        // "Online",
		        "Total pendaftar",
		    ],
		    datasets: [
		        {
		            data: [<?= $total_pengunjung;?>, <?= $pengunjung_hari_ini;?>, <?= $total_pendaftar;?>],
		            backgroundColor: [
		                "rgba(255, 159, 64, 1)",
		                "black",
		                "grey",
		                // "brown",
		            ]
		        }]
	        // datasets: [
	        // {
	        //     data: [<?= $total_pengunjung;?>, <?= $pengunjung_hari_ini;?>, <?= $pengunjung_online;?>, <?= $total_pendaftar;?>],
	        //     backgroundColor: [
	        //         "rgba(255, 159, 64, 1)",
	        //         "black",
	        //         "grey",
	        //         "brown",
	        //     ]
	        // }]
		};
		var chartOptions = {
		  rotation: -Math.PI,
		  cutoutPercentage: 30,
		  circumference: Math.PI,
		  legend: {
		    position: 'left'
		  },
		  animation: {
		    animateRotate: true,
		    animateScale: true
		  }
		};
        var pieChart = new Chart(ctx4, {
		    type: 'pie',
		    data: data,
		});

		$(function() {
		  $('#datepicker').datepicker({
		    changeYear: true,
		    showButtonPanel: true,
		    dateFormat: 'yy',
		    onClose: function(dateText, inst) {
		      var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		      $(this).datepicker('setDate', new Date(year, 1));
		    }
		  });

		  $("#datepicker").focus(function() {
		    $(".ui-datepicker-month").hide();
		    $(".ui-datepicker-calendar").hide();
		    $("div.ui-datepicker-header a.ui-datepicker-prev,div.ui-datepicker-header a.ui-datepicker-next").hide();
		    $("#ui-datepicker-div button.ui-datepicker-current").hide();
		  });

		});
</script>