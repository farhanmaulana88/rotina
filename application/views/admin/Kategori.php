   <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__text">
                        <h2>Kelola Produk</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="breadcrumb__links">
                        <a href="<?php echo base_url('admin/kategori');?>">Kategori</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Shopping Cart Section Begin -->
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table id="tabeluser" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">No. </th>
                                    <th width="75%">Kategori </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $no = 0;
                            foreach ($kategori as $key) { 
                            $no++;
                            ?>
                                <tr>
                                    <td class="cart__price"><?php echo $no;?></td>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__text">
                                            <h6><?php echo $key->kategori;?></h6>
                                        </div>
                                    </td>
                                    <td class="cart__close">
                                        <button class="btn btn-info btn-sm view_data_kategori" id="<?php echo $key->id; ?>"><i class="fa fa-pencil"></i></button>
                                        <a href="<?php echo base_url('admin/deleteKategori/'.$key->id);?>" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                                        
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="continue__btn update__btn">
                                <a href="#" data-toggle="modal" data-target="#createKategoriModal"><i class="fa fa-spinner"></i> Tambah Kategori </a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shopping Cart Section End -->

     <!-- Create Kategori Modal -->
    <div class="modal fade" id="createKategoriModal" tabindex="-1" role="dialog" aria-labelledby="produkModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="produkModalLabel"><b>Tambah Kategori</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="checkout__form">
                <form action="<?php echo base_url('admin/createKategori');?>" method="post">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Nama Kategori<span>*</span></p>
                                        <input type="text" name="kategori">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>

        </div>
      </div>
     </div>
    </div>

    <!-- Contoh Modal -->
    <div class="modal fade" id="kategoriModal" tabindex="-1" role="dialog" aria-labelledby="kategoriModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="kategoriModalLabel"><b>Update Kategori</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div id="kategoriHasil"></div>

        </div>
      </div>
     </div>
    </div>