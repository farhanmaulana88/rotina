<?php
Class Admin extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('ModelAdmin');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->load->helper('url');
        if ($this->session->userdata('role') != 'admin') {
            echo "<script>alert('Anda Tidak Memiliki Akses, Login Sebagai Admin Terlebih Dahulu!');history.go(-1);</script>";
            // redirect('main');
        }
    }

    function dashboard(){
        // print_r($this->input->post('tahun'));die;

        // if($thn == '') $thn = date('Y');
        ($this->input->post('tahun') != '') ? $tahun = $this->input->post('tahun') : $tahun = date('Y');
        $bulan = $this->ModelAdmin->getBulan()->result();

        $data['penjualan'] = [];
        $test = [];
        foreach ($bulan as $key) {
            $jml_penjualan=null;
            $jml_pendapatan=null;

            $cek_penjualan = $this->ModelAdmin->getTotalPenjualan($key->id, $tahun)->result();
            $test[]=$cek_penjualan;
            foreach ($cek_penjualan as $key2) {
                if (!empty($cek_penjualan)) {
                    $jml_penjualan += $key2->total;
                }
            }

            $cek_pendapatan = $this->ModelAdmin->getTotalPendapatan($key->id, $tahun)->result();
            foreach ($cek_pendapatan as $key3) {
                if (!empty($cek_pendapatan)) {
                    $jml_pendapatan += $key3->total;
                }
            }

            $key->penjualan = $jml_penjualan;
            $key->pendapatan = $jml_pendapatan;

            $data['penjualan'][]=$key;
        }
        // print_r($test);die;

        $data['produk'] = [];
        $produk = $this->ModelAdmin->getProduk();
        foreach ($produk as $produk) {
            $jml_produk=0;
            $cek_produk = $this->ModelAdmin->getTotalProduk($produk->id, $tahun)->result();
            foreach ($cek_produk as $value_produk) {
                if (!empty($value_produk)) {
                    $jml_produk += $value_produk->total;
                }
            }
            $produk->total = $jml_produk;
            $data['produk'][]=$produk;
        }
        // print_r($data['produk']);die;


        $pengunjunghariini  = $this->db->query("SELECT * FROM statistik WHERE EXTRACT(year FROM tanggal) = ".$tahun." AND tanggal='".date('Y-m-d')."' GROUP BY ip")->num_rows();
        $statistik = $this->db->query("SELECT COUNT(hits) as hits FROM statistik where EXTRACT(year FROM tanggal) = ".$tahun."")->row();     
        $totalpengunjung = isset($statistik->hits)?($statistik->hits):0;   
        $bataswaktu = time() - 300; 
        // print_r($bataswaktu);die;   
        $pengunjungonline  = $this->db->query("SELECT * FROM statistik WHERE online > '".$bataswaktu."' and EXTRACT(year FROM tanggal) = ".$tahun."")->num_rows();
        $pendaftar  = $this->db->query("SELECT * FROM user WHERE EXTRACT(year FROM tanggal) = ".$tahun." and role='pembeli'")->num_rows();
          
         
        $data['pengunjung_hari_ini'] = $pengunjunghariini;
        $data['total_pengunjung'] = $totalpengunjung;
        $data['pengunjung_online'] = $pengunjungonline;
        $data['total_pendaftar'] = $pendaftar;
        $data['years'] = range(2010, strftime("%Y", time()));

        $kode['kode'] = 'dashboard';
        $data['tahun'] = $tahun;
        $this->load->view('komponen_admin/header',$kode);
        $this->load->view('admin/dashboard',$data);
        $this->load->view('komponen_admin/footer');
    }

    function index(){

        $data['kode'] = 'produk';
        $data['produk'] = $this->ModelAdmin->getProduk();
        // var_dump($data['produk']);die;
        $this->load->view('komponen_admin/header',$data);
        $this->load->view('admin/produk',$data);
        $this->load->view('komponen_admin/footer');
    }

    function formCreateProduk(){
        $data['kode'] = 'produk';
        $data['kategori'] = $this->ModelAdmin->getKategori();
        $this->load->view('komponen_admin/header',$data);
        $this->load->view('admin/createProduk',$data);
        $this->load->view('komponen_admin/footer');
    }

    function createProduk(){
        $config['upload_path'] = './assets/GambarProduk';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('gambar_utama')){
            $result1 = $this->upload->data();
            $gambar_utama = $result1['file_name'];
         }else{
            $gambar_utama = '-';
         }

         if ($this->upload->do_upload('gambar_detail1')){
            $result2 = $this->upload->data();
            $gambar_detail1 = $result2['file_name'];
         }else{
            $gambar_detail1 = '-';
         }

         if ($this->upload->do_upload('gambar_detail2')){
            $result3 = $this->upload->data();
            $gambar_detail2 = $result3['file_name'];
         }
         else{
            $gambar_detail2 = '-';
         }

         if ($this->upload->do_upload('gambar_detail3')){
            $result4 = $this->upload->data();
            $gambar_detail3 = $result4['file_name'];
         }else{
            $gambar_detail3 = '-';
         }

         if ($this->upload->do_upload('gambar_detail4')){
            $result5 = $this->upload->data();
            $gambar_detail4 = $result5['file_name'];
         }else{
            $gambar_detail4 = '-';
         }

         if ($this->upload->do_upload('gambar_detail5')){
            $result6 = $this->upload->data();
            $gambar_detail5 = $result6['file_name'];
         }else{
            $gambar_detail5 = '-';
         }

        $harga_min_rp = str_replace("Rp. ","", $this->input->post('harga_min'));
        $harga_min = str_replace(".","", $harga_min_rp);

        $harga_max_rp = str_replace("Rp. ","", $this->input->post('harga_max'));
        $harga_max = str_replace(".","", $harga_max_rp);

        $data = array(
            'nama_produk' => $this->input->post('nama_produk'), 
            'id_kategori' => $this->input->post('id_kategori'), 
            'gramasi' => $this->input->post('gramasi'), 
            'ukuran' => $this->input->post('ukuran'), 
            'komposisi' => $this->input->post('komposisi'), 
            'rasa' => $this->input->post('rasa'), 
            'karakteristik' => $this->input->post('karakteristik'), 
            'expired' => $this->input->post('expired'), 
            'id_ketersediaan' => 1, 
            'harga_min' => $harga_min, 
            'harga_max' => $harga_max, 
            'stok' => $this->input->post('expired'), 
            'gambar_utama' => $gambar_utama, 
            'gambar_detail1' => $gambar_detail1, 
            'gambar_detail2' => $gambar_detail2,
            'gambar_detail3' => $gambar_detail3,
            'gambar_detail4' => $gambar_detail4,
            'gambar_detail5' => $gambar_detail5
        );

        // var_dump($data);die;
        $this->ModelAdmin->createProduk($data);
        redirect('admin');
    }

    function getDataProdukModal() {
        $id = $this->input->post('produkData');
        if(isset($id) and !empty($id)){
            $dataProduk = $this->ModelAdmin->getDataProdukModal($id);
            $kategori = $this->ModelAdmin->getKategori();
            $ketersediaan = $this->ModelAdmin->getKetersediaan();

            $hasil_kategori = '';
            foreach ($kategori as $hasil) {
            $selected = $dataProduk->id_kategori == $hasil->id ? 'selected="selected"' : '';
            $hasil_kategori .= '<option value="'.$hasil->id.'"'.$selected.'>'.$hasil->kategori.'</option>';
            }

            $hasil_ketersediaan = '';
            foreach ($ketersediaan as $ketersediaan) {
            $selected_ketersediaan = $dataProduk->id_ketersediaan == $ketersediaan->id ? 'selected="selected"' : '';
            $hasil_ketersediaan .= '<option value="'.$ketersediaan->id.'"'.$selected_ketersediaan.'>'.$ketersediaan->ketersediaan.'</option>';
            }

            // print_r($dataProduk->id);die;
            $urlGambar = base_url().'assets/GambarProduk/';

            if ($dataProduk->gambar_utama != '-') {
                $gambar_utama = $dataProduk->gambar_utama;
            }else{
                $gambar_utama = 'no-photo.jpg';
            }

            if ($dataProduk->gambar_detail1 != '-') {
                $gambar_detail1 = $dataProduk->gambar_detail1;
            }else{
                $gambar_detail1 = 'no-photo.jpg';
            }

            if ($dataProduk->gambar_detail2 != '-') {
                $gambar_detail2 = $dataProduk->gambar_detail2;
            }else{
                $gambar_detail2 = 'no-photo.jpg';
            }

            if ($dataProduk->gambar_detail3 != '-') {
                $gambar_detail3 = $dataProduk->gambar_detail3;
            }else{
                $gambar_detail3 = 'no-photo.jpg';
            }

            if ($dataProduk->gambar_detail4 != '-') {
                $gambar_detail4 = $dataProduk->gambar_detail4;
            }else{
                $gambar_detail4 = 'no-photo.jpg';
            }

            if ($dataProduk->gambar_detail5 != '-') {
                $gambar_detail5 = $dataProduk->gambar_detail5;
            }else{
                $gambar_detail5 = 'no-photo.jpg';
            }

            $harga_max = number_format($dataProduk->harga_max, 0, ".", ".");
            $harga_min = number_format($dataProduk->harga_min, 0, ".", ".");

            $output = '';

                 $output .= '      
             <div class="checkout__form">
                <form action="'.base_url('admin/updateProduk').'" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Nama Produk<span>*</span></p>
                                        <input type="text" value="'.$dataProduk->nama_produk.'" name="nama_produk">
                                        <input type="hidden" value="'.$dataProduk->id.'" name="id">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Kategori<span>*</span></p>
                                        <select name="id_kategori" class="form-control">
                                        '.$hasil_kategori.'
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gramasi<span>*</span></p>
                                        <input type="text" name="gramasi" value="'.$dataProduk->gramasi.'">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Ukuran<span>*</span></p>
                                        <input type="text" name="ukuran" value="'.$dataProduk->ukuran.'">
                                    </div>
                                </div>
                            </div>
                            <div class="checkout__input">
                                <p>Komposisi<span>*</span></p>
                                <textarea class="form-control" style="height:100px" name="komposisi" value="'.$dataProduk->komposisi.'">'.$dataProduk->komposisi.'</textarea>
                            </div>
                            <div class="checkout__input">
                                <p>Rasa<span>*</span></p>
                                <textarea class="form-control" style="height:100px" name="rasa" value="'.$dataProduk->rasa.'">'.$dataProduk->rasa.'</textarea>
                            </div>
                            <div class="checkout__input">
                                <p>Karakteristik<span>*</span></p>
                                <textarea class="form-control" style="height:100px" name="karakteristik" value="'.$dataProduk->karakteristik.'">'.$dataProduk->karakteristik.'</textarea>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Expired<span>*</span></p>
                                        <input type="text" name="expired" value="'.$dataProduk->expired.'">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Stok<span>*</span></p>
                                        <input type="number" name="stok" value="'.$dataProduk->stok.'">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Harga Minimal<span>*</span></p>
                                        <input id="min" type="text" name="harga_min" value="'.$harga_min.'">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Harga Maksimal<span>*</span></p>
                                        <input type="text" id="max" name="harga_max" value="'.$harga_max.'">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Utama<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar_utama.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar_utama" value="'.$dataProduk->gambar_utama.'">
                                        <input type="file" name="gambar_utama">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 1<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar_detail1.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar_detail1" value="'.$dataProduk->gambar_detail1.'">
                                        <input type="file" name="gambar_detail1">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 2<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar_detail2.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar_detail2" value="'.$dataProduk->gambar_detail2.'">
                                        <input type="file" name="gambar_detail2">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 3<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar_detail3.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar_detail3" value="'.$dataProduk->gambar_detail3.'">
                                        <input type="file" name="gambar_detail3">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 4<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar_detail4.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar_detail4" value="'.$dataProduk->gambar_detail4.'">
                                        <input type="file" name="gambar_detail4">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 5<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar_detail5.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar_detail5" value="'.$dataProduk->gambar_detail5.'">
                                        <input type="file" name="gambar_detail5">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>'
            ;

                echo $output;
        }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Pilih Produk'.'</li></ul></center>';
        }
    }

        function updateProduk(){
            // print_r($this->input->post('id'));die;
        $config['upload_path'] = './assets/GambarProduk';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('gambar_utama')){
            $result1 = $this->upload->data();
            $gambar_utama = $result1['file_name'];
         }else{
            $gambar_utama = $this->input->post('gambar_utama');
         }

         if ($this->upload->do_upload('gambar_detail1')){
            $result2 = $this->upload->data();
            $gambar_detail1 = $result2['file_name'];
         }else{
            $gambar_detail1 = $this->input->post('gambar_detail1');
         }

         if ($this->upload->do_upload('gambar_detail2')){
            $result3 = $this->upload->data();
            $gambar_detail2 = $result3['file_name'];
         }
         else{
            $gambar_detail2 = $this->input->post('gambar_detail2');
         }

         if ($this->upload->do_upload('gambar_detail3')){
            $result4 = $this->upload->data();
            $gambar_detail3 = $result4['file_name'];
         }else{
            $gambar_detail3 = $this->input->post('gambar_detail3');
         }

         if ($this->upload->do_upload('gambar_detail4')){
            $result5 = $this->upload->data();
            $gambar_detail4 = $result5['file_name'];
         }else{
            $gambar_detail4 = $this->input->post('gambar_detail4');
         }

         if ($this->upload->do_upload('gambar_detail5')){
            $result6 = $this->upload->data();
            $gambar_detail5 = $result6['file_name'];
         }else{
            $gambar_detail5 = $this->input->post('gambar_detail5');
         }

        $harga_min = str_replace(".","", $this->input->post('harga_min'));

        $harga_max = str_replace(".","", $this->input->post('harga_max'));

        $data = array(
            'nama_produk' => $this->input->post('nama_produk'), 
            'id_kategori' => $this->input->post('id_kategori'), 
            'gramasi' => $this->input->post('gramasi'), 
            'ukuran' => $this->input->post('ukuran'), 
            'komposisi' => $this->input->post('komposisi'), 
            'rasa' => $this->input->post('rasa'), 
            'karakteristik' => $this->input->post('karakteristik'), 
            'expired' => $this->input->post('expired'), 
            'harga_min' => $harga_min, 
            'harga_max' => $harga_max, 
            'stok' => $this->input->post('stok'),
            'gambar_utama' => $gambar_utama, 
            'gambar_detail1' => $gambar_detail1, 
            'gambar_detail2' => $gambar_detail2,
            'gambar_detail3' => $gambar_detail3,
            'gambar_detail4' => $gambar_detail4,
            'gambar_detail5' => $gambar_detail5
        );
        // print_r($data);die;
        $idProduk = $this->input->post('id');
        // var_dump($data);die;
        $this->ModelAdmin->updateProduk($data,$idProduk);
        redirect('admin');
    }

    function deleteProduk($id){
        $this->ModelAdmin->deleteProduk($id);
        redirect('admin');
    }

    // =========================================================================================================

    function kategori(){

        $data['kode'] = 'kategori';
        $data['kategori'] = $this->ModelAdmin->getKategori();
        $this->load->view('komponen_admin/header',$data);
        $this->load->view('admin/kategori',$data);
        $this->load->view('komponen_admin/footer');
    }

    function createKategori(){
        $dataKategori = array(
            'kategori' => $this->input->post('kategori'), 
        );
        // print_r($dataKategori);die;
        $this->ModelAdmin->createKategori($dataKategori);
        redirect('admin/kategori');
    }

    function getDataKategoriModal() {
        $id = $this->input->post('idKategori');
        if(isset($id) and !empty($id)){
            $dataKategori = $this->ModelAdmin->getDataKategoriModal($id);
            $output = '';

                 $output .= '      
             <div class="checkout__form">
                <form action="'.base_url('admin/updateKategori').'" method="post">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Nama Kategori<span>*</span></p>
                                        <input type="text" name="kategori" value="'.$dataKategori->kategori.'">
                                        <input type="hidden" name="id" value="'.$dataKategori->id.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>'
            ;

                echo $output;
        }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Pilih Produk'.'</li></ul></center>';
        }
    }

    function updateKategori(){
        $dataKategori = array(
            'kategori' => $this->input->post('kategori'), 
        );
        $idKategori = $this->input->post('id');
        // print_r($dataKategori);die;
        $this->ModelAdmin->updateKategori($dataKategori,$idKategori);
        redirect('admin/kategori');
    }

    function deleteKategori($idKategori){
        $this->ModelAdmin->deleteKategori($idKategori);
        redirect('admin/kategori');
    }

    function promo(){

        $data['kode'] = 'promo';
        $data['promo'] = $this->ModelAdmin->getPromo();
        // print_r($data['promo']);die;
        $this->load->view('komponen_admin/header',$data);
        $this->load->view('admin/promo',$data);
        $this->load->view('komponen_admin/footer');
    }

    function createPromo(){
        $config['upload_path'] = './assets/GambarPromo';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('gambar')){
            $result1 = $this->upload->data();
            $gambar = $result1['file_name'];
         }else{
            $gambar = '-';
         }

        $data = array(
            'nama_promo' => $this->input->post('nama_promo'), 
            'tanggal_mulai_promo' => $this->input->post('tanggal_mulai_promo'), 
            'tanggal_akhir_promo' => $this->input->post('tanggal_akhir_promo'), 
            'isi_promo' => $this->input->post('isi_promo'), 
            'voucher' => $this->input->post('voucher'), 
            'potongan' => $this->input->post('potongan'), 
            'min_belanja' => $this->input->post('min_belanja'), 
            'id_ketersediaan' => 1, 
            'gambar' => $gambar
        );

        // var_dump($data);die;
        $this->ModelAdmin->createPromo($data);
        redirect('admin/promo');
    }

    function getDataPromoModal() {
        $id = $this->input->post('idPromo');
        // print_r($id);die;
        if(isset($id) and !empty($id)){
            $dataPromo = $this->ModelAdmin->getDataPromoModal($id);
            $ketersediaan = $this->ModelAdmin->getKetersediaan();
            $hasil_ketersediaan = '';
            foreach ($ketersediaan as $ketersediaan) {
            $selected_ketersediaan = $dataPromo->id_ketersediaan == $ketersediaan->id ? 'selected="selected"' : '';
            $hasil_ketersediaan .= '<option value="'.$ketersediaan->id.'"'.$selected_ketersediaan.'>'.$ketersediaan->ketersediaan.'</option>';
            }
            $urlGambar = base_url().'assets/GambarPromo/';
            $gambar = $dataPromo->gambar;
            $output = '';

                 $output .= '      
                         <div class="checkout__form">
                <form action="'.base_url('admin/updatePromo').'" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Nama Promo<span>*</span></p>
                                        <input type="text" name="nama_promo" value="'.$dataPromo->nama_promo.'">
                                        <input type="hidden" name="id" value="'.$id.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Tanggal Mulai<span>*</span></p>
                                        <input type="date" name="tanggal_mulai_promo" value="'.$dataPromo->tanggal_mulai_promo.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Tanggal Selesai<span>*</span></p>
                                        <input type="date" name="tanggal_akhir_promo" value="'.$dataPromo->tanggal_akhir_promo.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                    <p>Tanggal Ketersediaan<span>*</span></p>
                                        <select class="form-control" name="ketersediaan">
                                            '.$hasil_ketersediaan.'
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Voucher</p>
                                        <input type="text" name="voucher" value="'.$dataPromo->voucher.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Potongan</p>
                                        <input type="text" name="potongan" value="'.$dataPromo->potongan.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Minimum Belanja</p>
                                        <input type="text" name="min_belanja" value="'.$dataPromo->min_belanja.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Isi Promo<span>*</span></p>
                                        <textarea class="summernotePromo" name="isi_promo">'.$dataPromo->isi_promo.'</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Gambar<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 180px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar" value="'.$dataPromo->gambar.'">
                                        <input type="file" class="form-control" name="gambar">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>
            <script>
                $(".summernotePromo").summernote({
              height: "300px",
              styleWithSpan: false
            });
            </script>'
            ;

                echo $output;
        }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Pilih Produk'.'</li></ul></center>';
        }
    }

        function updatePromo(){
        $config['upload_path'] = './assets/GambarPromo';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('gambar')){
            $result1 = $this->upload->data();
            $gambar = $result1['file_name'];
         }else{
            $gambar = $this->input->post('gambar');
         }

        $data = array(
            'nama_promo' => $this->input->post('nama_promo'), 
            'tanggal_mulai_promo' => $this->input->post('tanggal_mulai_promo'), 
            'tanggal_akhir_promo' => $this->input->post('tanggal_akhir_promo'), 
            'isi_promo' => $this->input->post('isi_promo'), 
            'voucher' => $this->input->post('voucher'), 
            'potongan' => $this->input->post('potongan'), 
            'min_belanja' => $this->input->post('min_belanja'), 
            'id_ketersediaan' => $this->input->post('ketersediaan'), 
            'gambar' => $gambar, 
        );
        $idProduk = $this->input->post('id');
        // var_dump($data);die;
        $this->ModelAdmin->updatePromo($data,$idProduk);
        redirect('admin/promo');
    }

    function deletePromo($id){
        $this->ModelAdmin->deletePromo($id);
        redirect('admin/promo');
    }

    // =======================================================================================================================================

    function konten(){

        $data['kode'] = 'konten';
        $data['instagram'] = $this->ModelAdmin->getInstagram();
        $data['team'] = $this->ModelAdmin->getTeam();
        $data['tentang'] = $this->ModelAdmin->getTentang();
        $data['social_media'] = $this->ModelAdmin->getSocialMedia();
        $data['lokasi'] = $this->ModelAdmin->getLokasi();
        $this->load->view('komponen_admin/header',$data);
        $this->load->view('admin/konten',$data);
        $this->load->view('komponen_admin/footer');
    }

    function getDataInstagramModal() {
        $id = $this->input->post('idInstagram');
        if(isset($id) and !empty($id)){
            $dataInstagram = $this->ModelAdmin->getDataInstagramModal($id);
            $urlGambar = base_url().'assets/instagram/';

            if ($dataInstagram->gambar1 != '-') {
                $gambar1 = $dataInstagram->gambar1;
            }else{
                $gambar1 = 'no-photo.jpg';
            }

            if ($dataInstagram->gambar2 != '-') {
                $gambar2 = $dataInstagram->gambar2;
            }else{
                $gambar2 = 'no-photo.jpg';
            }

            if ($dataInstagram->gambar3 != '-') {
                $gambar3 = $dataInstagram->gambar3;
            }else{
                $gambar3 = 'no-photo.jpg';
            }

            if ($dataInstagram->gambar4 != '-') {
                $gambar4 = $dataInstagram->gambar4;
            }else{
                $gambar4 = 'no-photo.jpg';
            }

            if ($dataInstagram->gambar5 != '-') {
                $gambar5 = $dataInstagram->gambar5;
            }else{
                $gambar5 = 'no-photo.jpg';
            }

            if ($dataInstagram->gambar6 != '-') {
                $gambar6 = $dataInstagram->gambar6;
            }else{
                $gambar6 = 'no-photo.jpg';
            }

            $output = '';

                 $output .= '      
             <div class="checkout__form">
                <form action="'.base_url('admin/updateInstagram').'" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Nama Produk<span>*</span></p>
                                        <input type="text" value="'.$dataInstagram->instagram.'" name="instagram">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Utama<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar1.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar1" value="'.$dataInstagram->gambar1.'">
                                        <input type="file" name="gambar1">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 1<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar2.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar2" value="'.$dataInstagram->gambar2.'">
                                        <input type="file" name="gambar2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 2<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar3.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar3" value="'.$dataInstagram->gambar3.'">
                                        <input type="file" name="gambar3">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 3<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar4.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar4" value="'.$dataInstagram->gambar4.'">
                                        <input type="file" name="gambar4">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 4<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar5.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar5" value="'.$dataInstagram->gambar5.'">
                                        <input type="file" name="gambar5">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Gambar Detail 5<span>*</span></p>
                                        <center>
                                        <img style="width: 290px; height: 230px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$gambar6.'" alt="img">
                                        </center>
                                        <input type="hidden" name="gambar6" value="'.$dataInstagram->gambar6.'">
                                        <input type="file" name="gambar6">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>'
            ;

                echo $output;
        }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Pilih Produk'.'</li></ul></center>';
        }
    }

        function updateInstagram(){
        $config['upload_path'] = './assets/instagram';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('gambar1')){
            $result1 = $this->upload->data();
            $gambar1 = $result1['file_name'];
         }else{
            $gambar1 = $this->input->post('gambar1');
         }

         if ($this->upload->do_upload('gambar2')){
            $result3 = $this->upload->data();
            $gambar2 = $result3['file_name'];
         }
         else{
            $gambar2 = $this->input->post('gambar2');
         }

         if ($this->upload->do_upload('gambar3')){
            $result4 = $this->upload->data();
            $gambar3 = $result4['file_name'];
         }else{
            $gambar3 = $this->input->post('gambar3');
         }

         if ($this->upload->do_upload('gambar4')){
            $result5 = $this->upload->data();
            $gambar4 = $result5['file_name'];
         }else{
            $gambar4 = $this->input->post('gambar4');
         }

         if ($this->upload->do_upload('gambar5')){
            $result6 = $this->upload->data();
            $gambar5 = $result6['file_name'];
         }else{
            $gambar5 = $this->input->post('gambar5');
         }

         if ($this->upload->do_upload('gambar6')){
            $result7 = $this->upload->data();
            $gambar6 = $result7['file_name'];
         }else{
            $gambar6 = $this->input->post('gambar6');
         }

        $data = array(
            'instagram' => $this->input->post('instagram'), 
            'gambar1' => $gambar1, 
            'gambar2' => $gambar2,
            'gambar3' => $gambar3,
            'gambar4' => $gambar4,
            'gambar5' => $gambar5,
            'gambar6' => $gambar6
        );
        $id = 1;
        $this->ModelAdmin->updateInstagram($data,$id);
        redirect('admin/konten');
    }

    // =======================================================================================================================================

    function createTeam(){
        $config['upload_path'] = './assets/Team';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto')){
            $result1 = $this->upload->data();
            $foto = $result1['file_name'];
         }else{
            $foto = '-';
         }

        $data = array(
            'nama' => $this->input->post('nama'), 
            'jabatan' => $this->input->post('jabatan'), 
            'facebook' => $this->input->post('facebook'), 
            'twitter' => $this->input->post('twitter'), 
            'instagram' => $this->input->post('instagram'), 
            'youtube' => $this->input->post('youtube'), 
            'foto' => $foto
        );

        // var_dump($data);die;
        $this->ModelAdmin->createTeam($data);
        redirect('admin/konten');
    }

    function getDataTeamModal() {
        $id = $this->input->post('idTeam');
        if(isset($id) and !empty($id)){
            $dataTeam = $this->ModelAdmin->getDataTeamModal($id);
            $urlGambar = base_url().'assets/Team/';
            $foto = $dataTeam->foto;
            $output = '';

                 $output .= '      
                         <div class="checkout__form">
                <form action="'.base_url('admin/updateTeam').'" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Nama<span>*</span></p>
                                        <input type="text" name="nama" value="'.$dataTeam->nama.'" required>
                                        <input type="hidden" name="id" value="'.$id.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Jabatan<span>*</span></p>
                                        <input type="text" name="jabatan" value="'.$dataTeam->jabatan.'" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Facebook</p>
                                        <input type="text" name="facebook" value="'.$dataTeam->facebook.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Twitter</p>
                                        <input type="text" name="twitter" value="'.$dataTeam->twitter.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Instagram</p>
                                        <input type="text" name="instagram" value="'.$dataTeam->instagram.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Youtube</p>
                                        <input type="text" name="youtube" value="'.$dataTeam->youtube.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Foto<span>*</span></p>
                                        <center>
                                        <img style="width: 180px; height: 220px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambar.''.$foto.'" alt="img">
                                        </center>
                                        <input type="hidden" name="foto" value="'.$dataTeam->foto.'">
                                        <input type="file" class="form-control" name="foto">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>'
            ;

                echo $output;
        }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Pilih Produk'.'</li></ul></center>';
        }
    }

        function updateTeam(){
        $config['upload_path'] = './assets/Team';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto')){
            $result1 = $this->upload->data();
            $foto = $result1['file_name'];
         }else{
            $foto = $this->input->post('foto');
         }

        $data = array(
            'nama' => $this->input->post('nama'), 
            'jabatan' => $this->input->post('jabatan'), 
            'facebook' => $this->input->post('facebook'), 
            'twitter' => $this->input->post('twitter'), 
            'instagram' => $this->input->post('instagram'), 
            'youtube' => $this->input->post('youtube'), 
            'foto' => $foto
        );
        $id = $this->input->post('id');
        $this->ModelAdmin->updateTeam($data,$id);
        redirect('admin/konten');
    }

    function deleteTeam($id){
        $this->ModelAdmin->deleteTeam($id);
        redirect('admin/konten');
    }

    // ==================================================================================================================================

    function createLokasi(){
        $data = array(
            'kota' => $this->input->post('kota'), 
            'alamat' => $this->input->post('alamat'), 
            'no_telp' => $this->input->post('no_telp'), 
            'email' => $this->input->post('email'), 
        );

        // var_dump($data);die;
        $this->ModelAdmin->createLokasi($data);
        redirect('admin/konten');
    }

    function getDataLokasiModal() {
        $id = $this->input->post('id');
        if(isset($id) and !empty($id)){
            $dataLokasi = $this->ModelAdmin->getDataLokasiModal($id);
            $output = '';

                 $output .= '      
                        <div class="checkout__form">
                <form action="'.base_url('admin/updateLokasi').'" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Nama Kota/Kabupaten<span>*</span></p>
                                        <input type="text" name="kota" value="'.$dataLokasi->kota.'">
                                        <input type="hidden" name="id" value="'.$id.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Alamat<span>*</span></p>
                                        <textarea style="height:200px" class="form-control" name="alamat" value="'.$dataLokasi->alamat.'">'.$dataLokasi->alamat.'</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>No. Telpon</p>
                                        <input type="text" class="form-control" name="no_telp" value="'.$dataLokasi->no_telp.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Email</p>
                                        <input type="text" class="form-control" name="email" value="'.$dataLokasi->email.'">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>';

                echo $output;
        }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Pilih Produk'.'</li></ul></center>';
        }
    }

    function updateLokasi(){
        $data = array(
            'kota' => $this->input->post('kota'), 
            'alamat' => $this->input->post('alamat'), 
            'no_telp' => $this->input->post('no_telp'), 
            'email' => $this->input->post('email'), 
        );
        $id = $this->input->post('id');
        $this->ModelAdmin->updateLokasi($data,$id);
        redirect('admin/konten');
    }

    function deleteLokasi($id){
        $this->ModelAdmin->deleteLokasi($id);
        redirect('admin/konten');
    }

    // ==================================================================================================================================

        function updateTentang(){

        $data = array(
            'deskripsi' => $this->input->post('deskripsi'), 
            'visi' => $this->input->post('visi'), 
            'misi' => $this->input->post('misi'), 
            'petinggi' => $this->input->post('petinggi'), 
            'pekerja' => $this->input->post('pekerja'), 
            'tanggal_berdiri' => $this->input->post('tanggal_berdiri'), 
        );

        $data_social_media = array(
            'facebook' => $this->input->post('facebook'), 
            'twitter' => $this->input->post('twitter'), 
            'instagram' => $this->input->post('instagram'), 
        );
        $id = 1;
        $this->ModelAdmin->updateTentang($data,$id);
        $this->ModelAdmin->updateSocialMedia($data_social_media,$id);
        redirect('admin/konten');
    }



    // =======================================================================================================================================

    function pesanan(){

        $data['kode'] = 'pesanan';
        $data['pesanan'] = $this->ModelAdmin->getPembayaran();
        // print_r($data['pesanan']);die;
        $this->load->view('komponen_admin/header',$data);
        $this->load->view('admin/pesanan',$data);
        $this->load->view('komponen_admin/footer');
    }

     function getDataPembayaranModal() {
        $id = $this->input->post('idPesanan');
        if(isset($id) and !empty($id)){
            $dataPembayaran = $this->ModelAdmin->getDataPembayaranModal($id);
            $dataStatus = $this->ModelAdmin->getStatus();
            // print_r($dataPembayaran);die;
            $urlGambarUser = base_url().'assets/User/';

            $status = '';
            foreach ($dataStatus as $dataStatus) {
            $selected_status = $dataPembayaran->status == $dataStatus->id ? 'selected="selected"' : '';
            $status .= '<option value="'.$dataStatus->id.'"'.$selected_status.'>'.$dataStatus->status.'</option>';
            }

            if ($dataPembayaran->foto != '') {
                $foto = $dataPembayaran->foto;
            }else{
                $foto = 'no-photo.jpg';
            }

            $output = '';

                 $output .= '      
            <div class="checkout__form">
                <form action="'.base_url('admin/updatePembayaran').'" method="post"">
                    <div class="row">
                        <div class="col-lg-12 col-md-8">
                        <div class="row">
                                <div class="col-lg-12">
                                    <div class="checkout__input">
                                        <p>Foto Pemesan</p>
                                        <center>
                                        <img style="width: 200px; height: 200px; border: 4px solid #ec7e1f; padding: 10px;" src="'.$urlGambarUser.''.$foto.'" alt="img">
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Nomor Pesanan</p>
                                        <input type="text" value="'.$dataPembayaran->no_pesanan.'" disabled>
                                        <input type="hidden" value="'.$dataPembayaran->id.'" name="id">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Nama Pembeli</p>
                                        <input type="text" value="'.$dataPembayaran->nama.'" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Total Pembayaran</p>
                                        <input type="text" value="Rp. '.number_format($dataPembayaran->total).'" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>Status Pembayaran</p>
                                        <select name="status" class="form-control">
                                        '.$status.'
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="checkout__input">
                                <p>Alamat</p>
                                <textarea class="form-control" style="height:100px"value="'.$dataPembayaran->alamat.'" disabled>'.$dataPembayaran->alamat.'</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-dark">SIMPAN</button>
                  </div>
                </form>
            </div>'
            ;

                echo $output;
        }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Pilih Produk'.'</li></ul></center>';
        }
    }

    function updatePembayaran(){
        $data = array(
            'status' => $this->input->post('status')
        );
        $id = $this->input->post('id');
        $this->ModelAdmin->updateStatus($data,$id);
        redirect('admin/pesanan');
    }

    function getDataPembelianModal() {
        $id = $this->input->post('idPembelian');
        if(isset($id) and !empty($id)){
            $dataPembelian = $this->ModelAdmin->getDataPembelianModal($id);
            $dataPembayaran = $this->ModelAdmin->getDataPembayaranModal($id);
            $urlGambarProduk = base_url().'assets/GambarProduk/';
            $output = '';

                 $output .= '      
             <div class="breadcrumb-option">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="breadcrumb__text">
                                <h2>No. Pesanan '.strtoupper($dataPembayaran->no_pesanan).'</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="shopping-cart spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="shopping__cart__table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Product </th>
                                        <th><center>Kategori </center></th>
                                        <th><center>Quantity </center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                ';

                                foreach ($dataPembelian as $items) {
                                    if ($items['gambar_utama'] != '-') {
                                        $gambar_utama = $items['gambar_utama'];
                                    }else{
                                        $gambar_utama = 'no-photo.jpg';
                                    }

                                    $output .='
                                <tr>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__pic">
                                            <img style="width: 100px; height: 100px" src="'.$urlGambarProduk.$gambar_utama.'" alt="">
                                        </div>
                                        <div class="product__cart__item__text">
                                            <h6>'.$items['nama_produk'].'</h6>
                                        </div>
                                    </td>
                                    <td class="cart__price"><center>'.$items['kategori'].'</center></td>
                                    <td class="cart__price"><center>'.$items['jumlah'].'</center></td>
                                </tr>';
                                }
                                $output .='
                                </tbody>
                            </table>
                        </div>
                    </div>        
                </div>
            </div>
        </section>      
        ';
            ;

                echo $output;
        }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Pilih Produk'.'</li></ul></center>';
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('main');
    }
}