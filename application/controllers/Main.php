<?php
Class Main extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('cart');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('date');
        $this->load->model('ModelMain');
        $this->load->model('ModelAdmin');
        if ($this->session->userdata('role') == 'admin') {
            redirect('admin');
        }
    }

    function registrasi(){
        // print_r($this->input->post());die;
        $config['upload_path'] = './assets/User';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto')){
            $result1 = $this->upload->data();
            $foto = $result1['file_name'];
         }else{
            $foto = $this->input->post('foto');
         }

        $data = array(
            'username' => $this->input->post('username'), 
            'password' => $this->input->post('password'), 
            'nama' => $this->input->post('nama'), 
            'alamat' => $this->input->post('alamat'),
            'foto' => $foto,
            'role' => 'pembeli',
            'tanggal' => date('Y-m-d'),
        );

        $input = $this->ModelMain->createUser($data);
        $login = array('username' => $this->input->post('username'),
                      'password' => $this->input->post('password')
            );
        $cek = $this->ModelMain->login($login);
        foreach ($cek->result() as $sess) {
            $sess_data['id'] = $sess->id;
            $sess_data['role'] = $sess->role;
            $sess_data['nama'] = $sess->nama;
            $sess_data['status'] = 'login';
            $this->session->set_userdata($sess_data);
        }
        echo "<script>history.go(-1);</script>";
    }

    function cek_login(){
        $data = array('username' => $this->input->post('username'),
                      'password' => $this->input->post('password')
            );
        $cek = $this->ModelMain->login($data);
        if ($cek->num_rows() == 1) {
            foreach ($cek->result() as $sess) {
                $sess_data['id'] = $sess->id;
                $sess_data['role'] = $sess->role;
                $sess_data['nama'] = $sess->nama;
                $sess_data['status'] = 'login';
                $this->session->set_userdata($sess_data);
            }
            if ($this->session->userdata('role')=='admin') {
                // print_r('admin');die;
                redirect('admin');
            }
            elseif ($this->session->userdata('role')=='pembeli') {
                echo "<script>history.go(-1);</script>";
            }   
        }else{
            echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
        }
        
    }


    function logout(){
        $this->cart->destroy();
        $this->session->sess_destroy();
        redirect('main');
    }

    function getDataUserModal() {
        $id = $this->input->post('idUser');
        if(isset($id) and !empty($id)){
            $dataUser = $this->ModelMain->getDataUserModal($id);
            $urlGambar = base_url().'assets/user/';

            if ($dataUser->foto != '') {
                $foto = $dataUser->foto;
            }else{
                $foto = 'no-photo.jpg';
            }
            $output = '';
            $output .= '      
            <form method="post" action="'.base_url('main/updateUser').'" enctype="multipart/form-data">
                <center><img src="'.base_url('assets/cake-main/img/rotina_logo/logo.png').'"></center>
                <center><img style="width:150px; height:150px;" src="'.$urlGambar.''.$foto.'">
                <input type="hidden" name="foto" value="'.$dataUser->foto.'">
                <input type="hidden" name="id" value="'.$dataUser->id.'"><br><br>
                <input type="file" name="foto"></center><br>
                <div class="form-group" align="left">
                  <label><img src="'.base_url('assets/cake-main/img/icon/user.png').'"> Username:</label><br>
                  <input type="text" class="form-control" name="username" value="'.$dataUser->username.'" required>
                </div>
                <div class="form-group" align="left">
                  <label><img src="'.base_url('assets/cake-main/img/icon/password.png').'"> Password:</label>
                  <input type="password" class="form-control" name="password" value="'.$dataUser->password.'" required>
                </div>
                <div class="form-group" align="left">
                  <label><img src="'.base_url('assets/cake-main/img/icon/nama.png').'"> Nama Lengkap:</label>
                  <input type="text" class="form-control" name="nama" value="'.$dataUser->nama.'" required>
                </div>
                <div class="form-group" align="left">
                  <label><img src="'.base_url('assets/cake-main/img/icon/alamat.png').'"> Alamat Lengkap:</label>
                  <textarea style="height:100px;" class="form-control" name="alamat" value="'.$dataUser->alamat.'" required>'.$dataUser->alamat.'</textarea>
                </div>
                <div class="modal-footer">
                  <a href="'.base_url('main/logout').'" type="submit" class="btn"><img src="'.base_url('assets/cake-main/img/icon/logout.png').'"></a>
                  <button type="submit" class="btn btn-dark">Update</button>
                </div>
            </form>'
            ;

                echo $output;
        }
        else {
            echo '<center><ul class="list-group"><li class="list-group-item">'.'Pilih Produk'.'</li></ul></center>';
        }
    }

    function updateUser(){
        $config['upload_path'] = './assets/User';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto')){
            $result1 = $this->upload->data();
            $foto = $result1['file_name'];
         }else{
            $foto = $this->input->post('foto');
         }

        $data = array(
            'username' => $this->input->post('username'), 
            'password' => $this->input->post('password'), 
            'nama' => $this->input->post('nama'), 
            'alamat' => $this->input->post('alamat'), 
            'foto' => $foto
        );
        $id = $this->input->post('id');
        $this->ModelMain->updateUser($data,$id);
        redirect('main');
    }

    // ================================================================================================================

    public function formCart(){
        if (!empty($this->input->post('voucher'))) {
            $potongan = $this->ModelMain->getVoucher($this->input->post('voucher'))->row();
            // print_r($potongan);die;
            if (!empty($potongan)) {
                $minBelanja = $this->ModelMain->getVoucherMin($this->input->post('voucher'))->row();
                if (!empty($minBelanja)) {
                    $data['hasil_potongan'] = $this->cart->total() - $potongan->potongan;
                    $data['pesan'] = '<span style="color:green;">Dapat Menggunakan Voucher</span>';
                    $data['potongan'] = '-'.number_format($potongan->potongan);
                }else{
                    $data['hasil_potongan'] = $this->cart->total();
                    $data['pesan'] = '<span style="color:red;">Total Belanja Kurang Dari Ketentuan Promo</span>';
                    $data['potongan'] = '-';    
                }
            }else{
                $data['hasil_potongan'] = $this->cart->total();
                $data['pesan'] = '<span style="color:red;">Voucher Tidak Ada Yang Sesuai!</span>';
                $data['potongan'] = '-';
            }
        }else{
            $data['potongan'] = '-';
            $data['hasil_potongan'] = $this->cart->total();
            $data['pesan'] = '';
        }
        $data['voucher'] = $this->input->post('voucher');
        // print_r($data);die;
        $kode['kode'] = 'Cart';
        $kode['social_media'] = $this->ModelAdmin->getSocialMedia();
        $kode['nama_pembeli'] = $this->ModelMain->getNamaPembeli($this->session->userdata('id'));
        $this->load->view('komponen/header',$kode);
        $this->load->view('main/cart',$data);
        $this->load->view('komponen/footer',$kode);
    }

    function getDataCartModal() {
        if ($this->session->userdata('status') != 'login') {
            $button = '<a href="#" data-toggle="modal" data-target="#loginModal" class="modalLogin primary-btn">Proceed to checkout</a>';
        }else{
            $button = '<a href="'.base_url('main/order').'" class="primary-btn">Proceed to checkout</a>';
        }
        $output = '';
        $output .= '
        <div class="breadcrumb-option">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="breadcrumb__text">
                            <h2>Shopping cart</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th>Product / Harga</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            ';

                            foreach ($this->cart->contents() as $items) {
                            $output .='
                            <tr>
                            <form action="'.base_url('main/updateCart').'" method="post"
                            <input type="hidden" name="cart['.$items['id'].'][id]" value="'.$items['id'].'" />
                            <input type="hidden" id="rowid" name="cart['.$items['id'].'][rowid]" value="'.$items['rowid'].'" />
                            <input type="hidden" name="cart['.$items['id'].'][name]" value="'.$items['name'].'" />
                            <input type="hidden" name="cart['.$items['id'].'][price]" value="'.$items['price'].'" />
                            <input type="hidden" name="cart['.$items['id'].'][gambar]" value="'.$items['gambar'].'" />
                                        <td class="product__cart__item">
                                            <div class="product__cart__item__pic">
                                                <img style="width: 100px; height: 100px" src="'.base_url('assets/GambarProduk/'.$items['gambar']).'" alt="">
                                            </div>
                                            <div class="product__cart__item__text">
                                                <h6>'.$items['name'].'</h6>
                                                <h5>Rp . '.number_format($items['price']).'</h5>
                                            </div>
                                        </td>
                                        <td class="quantity__item">
                                            <div class="quantity">
                                                <div class="pro-qty">
                                                    <input type="number" id="qty" name="cart['.$items['id'].'][qty]" value="'.$items['qty'].'">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="cart__price">Rp. '.number_format($items['subtotal']).'</td>
                                        <td class="cart__close"><span id="'.$items['rowid'].'" class="deleteCart icon_close"></span></td>
                        </form>
                                    </tr>';
                            }
                            $output .='
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="continue__btn update__btn">
                                <a href="#" class="updateCart"><i class="fa fa-spinner"></i> Update cart</a>
                            </div>
                        </div>
                    </div>
                </div>        
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="cart__total">
                        <h6>Cart total</h6>
                        <ul>
                            <li>Total <span>Rp. '.number_format($this->cart->total()).'</span></li>
                        </ul>
                        '.$button.'
                    </div>
                </div>
            </div>
        </div>
    </section>      
        ';
    echo $output;
    }

    function randomString($length)
    {
        $str        = "";
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
        $max        = strlen($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function order(){
        // print_r($this->input->post('hasil_potongan'));die;
        if (empty($this->cart->contents())) {
            echo "<script>alert('Tidak Bisa Checkout, Keranjang Belanja Masih Kosong!');history.go(-1);</script>";
        }else{
            $no_pesanan = $this->randomString(10);
            $is_processed = $this->ModelMain->order($no_pesanan, $this->input->post('hasil_potongan'), $this->input->post('metode_pembayaran'));
            foreach ($this->cart->contents() as $key) {
                $id = $key['id'];
                $qtyAwal = $this->db->get_where('produk',array('id' => $id))->row();
                $qtyAwalFix = $qtyAwal->stok;
                $qty = $key['qty'];
                $qtyAkhir = $qtyAwalFix - $qty;
                $stok = $this->ModelMain->stok($id,$qtyAkhir);
            }

            if($is_processed){
                $this->cart->destroy();
                redirect(base_url('main'));
            }else{
                $this->session->set_flashdata('error','Gagal untuk memproses pesanan anda, tolong coba lagi!');
                redirect(base_url('main')); 
            }  
        }
    }

    function add_to_cart(){ //fungsi Add To Cart
        $id = $this->input->post('idProduk');
        $dataProduk = $this->ModelAdmin->getDataProdukModal($id);
        $data = array(
            'id' => $dataProduk->id, 
            'name' => $dataProduk->nama_produk, 
            'price' => $dataProduk->harga_max, 
            'qty' => 1, 
            'gambar' => $dataProduk->gambar_utama
        );
        // print_r($data);die;
        $this->cart->insert($data);
        $hasil['data'] = $data;
        $hasil['jumlah_barang'] = $this->countCart();
        $hasil['jumlah_harga'] = $this->subtotalCart();
        echo json_encode($hasil); //tampilkan cart setelah added
    }

    function countCart(){ //fungsi Add To Cart
         $rows = count($this->cart->contents());
         return $rows;
    }

    function countCart2(){ //fungsi Add To Cart
         $rows = count($this->cart->contents());
         echo $rows;
    }

    function subtotalCart(){ //fungsi Add To Cart
         $subtotal = 'Rp. '.number_format($this->cart->total());
         return $subtotal;
    }

    function subtotalCart2(){ //fungsi Add To Cart
         $subtotal = 'Rp. '.number_format($this->cart->total());
         echo $subtotal;
    }

    function updateCart(){
    $cart_info = $_POST['cart'];
    // var_dump($cart_info);die;
    foreach( $cart_info as $id => $cart) {
        $rowid = $cart['rowid'];
        $price = $cart['price'];
        $gambar = $cart['gambar'];
        $amount = $price * $cart['qty'];
        $qty = $cart['qty'];
        $data = array(
            'rowid' => $rowid,
            'price' => $price,
            'gambar' => $gambar,
            'amount' => $amount,
            'qty' => $qty
        );
        $this->cart->update($data);
    }

        redirect(base_url('main/formCart'));
    }

    function deleteCart(){ //fungsi untuk menghapus item cart
        $data = array(
            'rowid' => $this->input->post('row_id'), 
            'qty' => 0, 
        );
        $this->cart->update($data);
        return true;
    }
    // ======================================================================================================================

    function index(){
        $ip      = $_SERVER['REMOTE_ADDR']; // Mendapatkan IP komputer user
        $tanggal = date("Y-m-d"); // Mendapatkan tanggal sekarang
        $waktu   = date("Y-m-d H:i:s"); //
        $s = $this->ModelMain->getStatistik($ip,$tanggal);
        if($s->num_rows() == 0){
            $this->ModelMain->createStatistik($ip,$tanggal,$waktu);
        }else{
            $this->ModelMain->updateStatistik($ip,$tanggal,$waktu);
        }
        $kode['kode'] = 'index';
        $kode['social_media'] = $this->ModelAdmin->getSocialMedia();
        $kode['nama_pembeli'] = $this->ModelMain->getNamaPembeli($this->session->userdata('id'));
        // print_r($kode['nama_pembeli']);die;
        $data['promo_thumbnail'] = $this->ModelMain->getDetailPromoThumbnail();
        $data['produk'] = $this->ModelMain->getProdukMain();
        $data['instagram'] = $this->ModelAdmin->getDataInstagramModal(1);
        $data['team'] = $this->ModelAdmin->getTeam();
        $data['leader'] = $this->ModelAdmin->getLeader();
        $this->load->view('komponen/header',$kode);
        $this->load->view('main/index',$data);
        $this->load->view('komponen/footer',$kode);
    }

    function produk(){
        $kode['kode'] = 'produk';
        $kode['social_media'] = $this->ModelAdmin->getSocialMedia();
        $kode['nama_pembeli'] = $this->ModelMain->getNamaPembeli($this->session->userdata('id'));
        $data['kategori'] = $this->ModelMain->getKategori();
        $id_kategori = ($this->input->post('id_kategori')) ? $this->input->post('id_kategori') : '';
        $pencarian = ($this->input->post('pencarian')) ? $this->input->post('pencarian') : '';
        // print_r($pencarian);die;
        if ($id_kategori != '' or $pencarian != '') {
            $config['total_rows'] = $this->ModelMain->getJumlahProduk($id_kategori,$pencarian);
            $data['produk'] = $this->ModelMain->getProduk('','',$id_kategori,$pencarian);
            $data['keseluruhan'] = $config['total_rows'];
            $data['didapat'] = '';
            $data['id_kategori'] = $id_kategori;
            $data['pencarian'] = $pencarian;
        }else{
            $data['kategori'] = $this->ModelMain->getKategori();
            $config['base_url'] = base_url().'main/produk/';
            $config['total_rows'] = $this->ModelMain->getJumlahProduk($id_kategori,$pencarian);
            $config['per_page'] =4;
            $config["uri_segment"] = 3;  // uri parameter
            $choice = $config["total_rows"] / $config["per_page"];
            $config['num_links'] = floor($choice);
            $config['reuse_query_string'] = TRUE;
                 
            $config['full_tag_open'] = "<div class='shop__pagination'>";
            $config['full_tag_close'] = '</div>';
            $config['cur_tag_open'] = '<a style="background-color:#ec7e1f;text-decoration: none;">';
            $config['cur_tag_close'] = '</a>';
            $config['prev_tag_open'] = '<a>';
            $config['prev_tag_close'] = '</a>';
            $config['first_tag_open'] = '<a>';
            $config['first_tag_close'] = '</a>';
            $config['last_tag_open'] = '<a>';
            $config['last_tag_close'] = '</a>';

            $config['prev_link'] = '<i class="arrow_carrot-left"></i>';
            $config['prev_tag_open'] = '';
            $config['prev_tag_close'] = '';


            $config['next_link'] = '<i class="arrow_carrot-right"></i>';
            $config['next_tag_open'] = '';
            $config['next_tag_close'] = '';
             
            $this->pagination->initialize($config);
            $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['produk'] = $this->ModelMain->getProduk($config['per_page'],$data['page'],$id_kategori,$pencarian);
            $data['pagination'] = $this->pagination->create_links();
            $data['x'] = (int)$this->uri->segment(3) + 1;
            if ($this->uri->segment(3) + $config['per_page'] > $config['total_rows']) {
                $data['y'] = $config['total_rows'];
            } else {
                $data['y'] = (int)$this->uri->segment(3) + $config['per_page'];
            }
            $data['keseluruhan'] = $config['total_rows'];
            $data['didapat'] = $data['x'].' - '.$data['y'].' dari';
            $data['id_kategori'] = $id_kategori;
            $data['pencarian'] = $pencarian;
        }

        $this->load->view('komponen/header',$kode);
        $this->load->view('main/produk',$data);
        $this->load->view('komponen/footer',$kode);
    }

    function produkDetail($id,$kategori){
        // print_r($id);die;
        $kode['kode'] = 'produk';
        $kode['social_media'] = $this->ModelAdmin->getSocialMedia();
        $kode['nama_pembeli'] = $this->ModelMain->getNamaPembeli($this->session->userdata('id'));
        $data['produk'] = $this->ModelMain->getDetailProdukMain($id);
        $data['produk_terkait'] = $this->ModelMain->getDetailProdukTerkaitMain($kategori)->result();
        // print_r($data['produk_terkait']);die;
        $this->load->view('komponen/header',$kode);
        $this->load->view('main/produkDetail',$data);
        $this->load->view('komponen/footer',$kode);
    }

    function promo(){
        $kode['kode'] = 'promo';
        $kode['social_media'] = $this->ModelAdmin->getSocialMedia();
        $kode['nama_pembeli'] = $this->ModelMain->getNamaPembeli($this->session->userdata('id'));
        $pencarian = ($this->input->post('pencarian')) ? $this->input->post('pencarian') : '';
        // print_r($pencarian);die;
        if ($pencarian != '') {
            $config['total_rows'] = $this->ModelMain->getJumlahPromo($pencarian);
            $data['promo'] = $this->ModelMain->getPromo('','',$pencarian);
            $data['keseluruhan'] = $config['total_rows'];
            $data['didapat'] = '';
            $data['pencarian'] = $pencarian;
        }else{
            $config['base_url'] = base_url().'main/promo/';
            $config['total_rows'] = $this->ModelMain->getJumlahPromo();
            $config['per_page'] =2;
            $config['uri_segment'] = 3;  // uri parameter
            $choice = $config['total_rows'] / $config['per_page'];
            $config['num_links'] = floor($choice);
            $config['reuse_query_string'] = TRUE;
                 
            $config['full_tag_open'] = "<div class='shop__pagination'>";
            $config['full_tag_close'] = '</div>';
            $config['cur_tag_open'] = '<a style="background-color:#ec7e1f;text-decoration: none;">';
            $config['cur_tag_close'] = '</a>';
            $config['prev_tag_open'] = '<a>';
            $config['prev_tag_close'] = '</a>';
            $config['first_tag_open'] = '<a>';
            $config['first_tag_close'] = '</a>';
            $config['last_tag_open'] = '<a>';
            $config['last_tag_close'] = '</a>';

            $config['prev_link'] = '<i class="arrow_carrot-left"></i>';
            $config['prev_tag_open'] = '';
            $config['prev_tag_close'] = '';


            $config['next_link'] = '<i class="arrow_carrot-right"></i>';
            $config['next_tag_open'] = '';
            $config['next_tag_close'] = '';
             
            $this->pagination->initialize($config);
            $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['promo'] = $this->ModelMain->getPromo($config['per_page'],$data['page'],$pencarian);
            $data['pagination'] = $this->pagination->create_links();
            $data['x'] = (int)$this->uri->segment(3) + 1;
            if ($this->uri->segment(3) + $config['per_page'] > $config['total_rows']) {
                $data['y'] = $config['total_rows'];
            } else {
                $data['y'] = (int)$this->uri->segment(3) + $config['per_page'];
            }
            $data['keseluruhan'] = $config['total_rows'];
            $data['didapat'] = $data['x'].' - '.$data['y'].' dari';
            $data['pencarian'] = $pencarian;
        }
        $this->load->view('komponen/header',$kode);
        $this->load->view('main/promo',$data);
        $this->load->view('komponen/footer',$kode);
    }

    function promoDetail($id){
        $kode['kode'] = 'promo';
        $kode['social_media'] = $this->ModelAdmin->getSocialMedia();
        $kode['nama_pembeli'] = $this->ModelMain->getNamaPembeli($this->session->userdata('id'));
        $data['promo'] = $this->ModelMain->getDetailPromo($id);
        $data['promo_thumbnail'] = $this->ModelMain->getDetailPromoThumbnail();
        // print_r($data['promo_thumbnail']);die;
        $this->load->view('komponen/header',$kode);
        $this->load->view('main/promoDetail',$data);
        $this->load->view('komponen/footer',$kode);
    }

    function tentang(){
        $kode['kode'] = 'tentang';
        $kode['social_media'] = $this->ModelAdmin->getSocialMedia();
        $kode['nama_pembeli'] = $this->ModelMain->getNamaPembeli($this->session->userdata('id'));
        $data['tentang'] = $this->ModelAdmin->getTentang();
        $this->load->view('komponen/header',$kode);
        $this->load->view('main/tentang',$data);
        $this->load->view('komponen/footer',$kode);
    }

    function kontak(){
        $kode['kode'] = 'kontak';
        $kode['social_media'] = $this->ModelAdmin->getSocialMedia();
        $kode['nama_pembeli'] = $this->ModelMain->getNamaPembeli($this->session->userdata('id'));
        $data['lokasi'] = $this->ModelAdmin->getLokasi();
        $this->load->view('komponen/header',$kode);
        $this->load->view('main/kontak',$data);
        $this->load->view('komponen/footer',$kode);
    }

    function getDataListModal() {
        $id = $this->input->post('id');
        $dataPembelian = $this->ModelMain->getDataListModal($id);
        // print_r($dataPembelian);die;
        $urlGambarProduk = base_url().'assets/GambarProduk/';
        $output = '';

             $output .= '     
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"> 
 
            
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="breadcrumb__text">
                            <h2>Riwayat Belanja</h2>
                        </div>
                    </div>
                </div>

        <br>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table id="tabelList">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="40%">Product </th>
                                    <th width="5%">Jumlah </th>
                                    <th width="10%">No Pesanan </th>
                                    <th width="20%">Tanggal Pembelian</th>
                                    <th width="20%">Status </th>
                                </tr>
                            </thead>
                            <tbody>
                            ';
                            if ($dataPembelian->num_rows() > 0) {
                                $no=0;
                                foreach ($dataPembelian->result() as $items) {
                                    if ($items->gambar_utama != '-') {
                                        $gambar_utama = $items->gambar_utama;
                                    }else{
                                        $gambar_utama = 'no-photo.jpg';
                                    }
                                    $no++;

                                    $output .='
                                <tr>
                                    <td class="cart__price">'.$no.'</td>
                                    <td class="product__cart__item">
                                        <div class="product__cart__item__pic">
                                            <img style="width: 100px; height: 100px" src="'.$urlGambarProduk.$gambar_utama.'" alt="">
                                        </div>
                                        <div class="product__cart__item__text">
                                            <h6>'.$items->nama_produk.'</h6>
                                        </div>
                                    </td>
                                    <td class="cart__price"><center>'.$items->jumlah.'</center></td>
                                    <td class="cart__price"><center>'.$items->no_pesanan.'</center></td>
                                    <td class="cart__price"><center>'.date('d-M-Y', strtotime($items->tanggal)).'</center></td>
                                    <td class="cart__price"><center>'.$items->status.'</center></td>
                                </tr>';
                                }
                            }else{
                                $output .='
                                <tr>
                                    <td colspan="5">Anda Belum Melakukan Pembelian!</td>
                                </tr>';
                            }
                            $output .='
                            </tbody>
                        </table>
                    </div>
                </div>        
            </div>
        </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $("#tabelList").dataTable( {
          "pageLength": 5
        } );
      });
    </script>   
    ';

        echo $output;
    }

    function getDataBillModal() {
        $id = $this->input->post('id');
        $dataPembayaran = $this->ModelMain->getDataBillModal($id);
        // print_r($dataPembayaran->result());die;
        $urlGambar = base_url().'assets/BuktiPembayaran/';
        $output = '';

             $output .= '     
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"> 
        <link rel="stylesheet" href="<?php echo base_url();?>assets/cake-main/css/jquery.fancybox.css?v=2.1.5" media="screen" type="text/css">
        <script src="<?php echo base_url();?>assets/cake-main/js/jquery.fancybox.js?v=2.1.5"></script>
        <script type="text/javascript">
            $(document).ready(function(){
             $(".perbesar").fancybox();
         });
        </script>
 
            
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="breadcrumb__text">
                            <h2>Daftar & Riwayat Pembayaran </h2>
                        </div>
                    </div>
                </div>

        <br>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <table id="tabelBill">
                            <thead>
                                <tr>
                                    <th><center>No Pesanan </center></th>
                                    <th><center>Total </center></th>
                                    <th><center>Pembayaran </center></th>
                                    <th><center>Tanggal Pembelian</center></th>
                                    <th><center>Status </center></th>
                                    <th><center>Upload</center></th>
                                </tr>
                            </thead>
                            <tbody>
                            ';
                            if ($dataPembayaran->num_rows() > 0) {
                                $no=0;
                                foreach ($dataPembayaran->result() as $items) {
                                    if ($items->bukti != '') {
                                        $bukti = $items->bukti;
                                    }else{
                                        $bukti = 'no-photo.jpg';
                                    }

                                    if ($items->metode_pembayaran != 'Langsung') {
                                        $pembayaran = '<td><center>
                                        <a href="'.$urlGambar.$bukti.'" class="perbesar">
                                            <img id="myImg" style="width: 100px; height: 100px" src="'.$urlGambar.$bukti.'"
                                        </a>
                                        </center></td>';
                                        $aksi = '
                                                <td>
                                                    <form enctype="multipart/form-data" method="POST" action="'.base_url("main/upload").'">
                                                        <input type="hidden" value="'.$items->id.'" name="id">
                                                        <input class="form-control" type="file" name="bukti" size="20">
                                                        <button type="submit"><i class="fa fa-pencil"></i></button>
                                                    </form>
                                                </td>
                                                ';
                                    }else{
                                        $pembayaran = '<td class="cart__price"><center>'.$items->metode_pembayaran.'</center></td>'; 
                                        $aksi = '<td></td>';
                                    }
                                    $no++;

                                    $output .='
                                <tr>
                                    <td class="cart__price"><center>'.$items->no_pesanan.'</center></td>
                                    <td class="cart__price"><center>Rp. '.number_format($items->total).'</center></td>
                                    '.$pembayaran.'
                                    <td class="cart__price"><center>'.date('d-M-Y', strtotime($items->tanggal)).'</center></td>
                                    <td class="cart__price"><center>'.$items->status.'</center></td>
                                    '.$aksi.'
                                </tr>';
                                }
                            }else{
                                $output .='
                                <tr>
                                    <td colspan="7">Anda Belum Melakukan Pembelian!</td>
                                </tr>';
                            }
                            $output .='
                            </tbody>
                        </table>
                    </div>
                </div>        
            </div>
        </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $("#tabelBill").dataTable( {
          "pageLength": 3,
        } );
      });
    </script>   
    ';

        echo $output;
    }

    function upload(){
        $config['upload_path'] = './assets/BuktiPembayaran';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';

         $this->load->library('upload', $config);
         if ($this->upload->do_upload('bukti')){
            $result1 = $this->upload->data();
            $foto = $result1['file_name'];
         }else{
            $foto = $this->input->post('bukti');
         }


        $data = array(
            'bukti' => $foto
        );
        $id = $this->input->post('id');
        $this->ModelMain->updateBuktiPembayaran($data,$id);
        echo "<script>history.go(-1);</script>";
    }

}