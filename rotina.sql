-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2020 at 02:50 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rotina`
--

-- --------------------------------------------------------

--
-- Table structure for table `bulan`
--

CREATE TABLE `bulan` (
  `id` int(11) NOT NULL,
  `bulan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulan`
--

INSERT INTO `bulan` (`id`, `bulan`) VALUES
(1, 'Januari'),
(2, 'Februari'),
(3, 'Maret'),
(4, 'April'),
(5, 'Mei'),
(6, 'Juni'),
(7, 'Juli'),
(8, 'Agustus'),
(9, 'September'),
(10, 'Oktober'),
(11, 'November'),
(12, 'Desember');

-- --------------------------------------------------------

--
-- Table structure for table `instagram`
--

CREATE TABLE `instagram` (
  `id` int(11) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  `gambar1` text NOT NULL,
  `gambar2` text NOT NULL,
  `gambar3` text NOT NULL,
  `gambar4` text NOT NULL,
  `gambar5` text NOT NULL,
  `gambar6` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instagram`
--

INSERT INTO `instagram` (`id`, `instagram`, `gambar1`, `gambar2`, `gambar3`, `gambar4`, `gambar5`, `gambar6`) VALUES
(1, '@rotina_tomofood', 'IMG_9819.jpg', 'IMG_9806.jpg', '1.jpg', '2.jpg', '71.jpg', '31.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`) VALUES
(1, 'ROTI TAWAR'),
(2, 'Roti Tawar Isi'),
(4, 'Bolu');

-- --------------------------------------------------------

--
-- Table structure for table `ketersediaan`
--

CREATE TABLE `ketersediaan` (
  `id` int(11) NOT NULL,
  `ketersediaan` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ketersediaan`
--

INSERT INTO `ketersediaan` (`id`, `ketersediaan`) VALUES
(1, 'TERSEDIA'),
(2, 'HABIS');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id` int(11) NOT NULL,
  `kota` varchar(40) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id`, `kota`, `alamat`, `no_telp`, `email`) VALUES
(1, 'Sumedang', 'Jalan Raya Cijelag-Cikamurang No.89 Desa Sakurjaya, Kec. Ujungjaya,Kab Sumedang 45383', '+620812345678', 'rotina_tomofood@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `no_pesanan` varchar(10) NOT NULL,
  `bukti` text NOT NULL,
  `total` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `metode_pembayaran` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `id_user`, `no_pesanan`, `bukti`, `total`, `status`, `tanggal`, `metode_pembayaran`) VALUES
(2, 2, 'vh2iONiMjc', '', 12000, '2', '2020-12-01', 'Langsung'),
(3, 2, 'aU83feFzpP', '', 45000, '2', '2020-10-01', 'Langsung'),
(6, 2, 'XiRgPMtfa3', '5677080_cf2ef623-8e05-4c90-80ec-0a6edb107d3e_648_6481.jpg', 75000, '2', '2020-12-24', 'Transfer'),
(7, 10, 'qGIU3eaI2U', '', 94000, '2', '2020-12-25', 'Langsung'),
(8, 2, 'EwSUyCZbpS', '5677080_cf2ef623-8e05-4c90-80ec-0a6edb107d3e_648_648.jpg', 50000, '1', '2020-12-25', 'Transfer'),
(9, 2, 'nJuYMR7Iwq', '', 45000, '1', '2020-12-25', 'Langsung'),
(10, 2, 'z91W5hgrf2', '', 45000, '1', '2020-12-26', 'Langsung');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `id_pembayaran` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id`, `id_pembayaran`, `id_user`, `id_produk`, `jumlah`, `status`) VALUES
(1, 2, 2, 10, 1, 'Diproses'),
(2, 2, 2, 12, 1, 'Diproses'),
(3, 2, 2, 12, 5, 'Diproses'),
(4, 2, 2, 11, 1, 'Diproses'),
(5, 3, 2, 18, 1, 'Diproses'),
(6, 3, 2, 16, 1, 'Diproses'),
(12, 6, 2, 18, 1, 'Diproses'),
(13, 6, 2, 17, 1, 'Diproses'),
(14, 6, 2, 16, 1, 'Diproses'),
(15, 6, 2, 13, 1, 'Diproses'),
(16, 7, 10, 7, 1, 'Diproses'),
(17, 7, 10, 8, 6, 'Diproses'),
(18, 7, 10, 10, 1, 'Diproses'),
(19, 8, 2, 16, 2, 'Diproses'),
(20, 9, 2, 16, 1, 'Diproses'),
(21, 9, 2, 13, 1, 'Diproses'),
(22, 10, 2, 18, 1, 'Diproses'),
(23, 10, 2, 17, 2, 'Diproses');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `harga_min` int(11) NOT NULL,
  `harga_max` int(11) NOT NULL,
  `gramasi` int(11) NOT NULL,
  `ukuran` varchar(20) NOT NULL,
  `rasa` text NOT NULL,
  `karakteristik` text NOT NULL,
  `komposisi` text NOT NULL,
  `expired` varchar(50) NOT NULL,
  `stok` int(11) NOT NULL,
  `id_ketersediaan` int(11) NOT NULL,
  `gambar_utama` text NOT NULL,
  `gambar_detail1` text NOT NULL,
  `gambar_detail2` text NOT NULL,
  `gambar_detail3` text NOT NULL,
  `gambar_detail4` text NOT NULL,
  `gambar_detail5` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `id_kategori`, `nama_produk`, `harga_min`, `harga_max`, `gramasi`, `ukuran`, `rasa`, `karakteristik`, `komposisi`, `expired`, `stok`, `id_ketersediaan`, `gambar_utama`, `gambar_detail1`, `gambar_detail2`, `gambar_detail3`, `gambar_detail4`, `gambar_detail5`) VALUES
(7, 2, 'Tawar Isi Coklat', 1800, 2000, 60, '60x50', 'coklat', 'coklat yang begitu manis dibuat oleh coklat kokona ', 'tepung,perisa coklat', '7 hari', 99, 1, 'IMG_9812.jpg', '-', 'coklat.jpg', '-', '-', '-'),
(8, 1, 'TAWAR PANJANG', 14000, 15000, 60, '60x50', 'ORIGINAL', 'ROTI TAWAR PANJANG YANG BEGITU LEMBUT', 'TEPUNG, TELUR,PERISA', '7 hari', 94, 1, 'IMG_9833.jpg', '-', '-', '-', '-', '-'),
(9, 2, 'TAWAR ISI SUSU', 1800, 2000, 60, '60x50', 'SUSU', 'SUSU YANG BEGITU MANIS DAN ENAK ', 'TEPUNG, TELUR,PERISA SUSU', '7 hari', 100, 2, 'IMG_9813.jpg', '-', 'susu.jpg', '-', '-', '-'),
(10, 2, 'TAWAR ISI KEJU', 1800, 2000, 60, '60x50', 'KEJU', 'KEJU YANG BEGITU GURIH ENAK ', 'TEPUNG, TELUR,PERISA KEJU', '7 hari', 98, 1, 'keju1.jpg', 'IMG_98142.jpg', 'keju.jpg', '-', '-', '-'),
(11, 2, 'TAWAR ISI BLUEBERI', 1800, 2000, 60, '60x50', 'BLUEBERI', 'BLUEBERI YANG SEGAR BERWARNA BIRU KEUNGU-UNGUAN ', 'TEPUNG, TELUR,PERISA BERI', '7 hari', 99, 2, 'IMG_9817.jpg', 'blueberi2.jpg', 'blueberi.jpg', 'IMG_98172.jpg', 'blueberi3.jpg', 'IMG_98173.jpg'),
(12, 2, 'TAWAR ISI STRAWBERI', 1800, 2000, 60, '60x50', 'STRAWBERI', 'STRAWBERI YANG BEGITU MANIS ASAM ENAK ', 'TEPUNG, TELUR,PERISA STRAWBEI', '7 hari', 94, 1, 'st.jpg', '-', 'strawberi.jpg', '-', '-', '-'),
(13, 1, 'TAWAR COKLAT', 15000, 15000, 60, '60x50', 'coklat', 'coklat yang begitu manis dibuat oleh coklat kokona ', 'tepung,perisa coklat', '7 hari', 97, 1, 'tcoklat2.jpg', 'tcoklat3.jpg', '-', '-', '-', '-'),
(16, 4, 'Amanda', 29000, 30000, 150, '100x70', 'coklat', 'coklat yang begitu manis dibuat oleh coklat kokona ', 'tepung,perisa coklat', '7 hari', 0, 1, '23.jpg', '31.jpg', '-', '-', '-', '-'),
(17, 1, 'TAWAR GANDUM', 14000, 15000, 150, '100x70', 'GANDUM', 'GANDUM YANG BAGUS UNTUK KESEHATAN,KHUSUSNYA UNTUK YANG DIET ', 'TEPUNG, TELUR,PERISA GANDUM', '7 hari', 0, 1, 'gandum2.jpg', 'gandumm2.jpg', '-', '-', '-', '-'),
(18, 1, 'TAWAR PANDAN', 14000, 15000, 150, '100x70', 'PANDAN', 'PANDAN YANG WANGI DAN WARNA YANG SANGAT ALAMI ', 'TEPUNG, TELUR,PERISA PANDAN', '7 hari', 4, 1, 'IMG_9828.jpg', 'IMG_9827.jpg', '-', '-', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--

CREATE TABLE `promo` (
  `id` int(11) NOT NULL,
  `nama_promo` text NOT NULL,
  `tanggal_mulai_promo` date NOT NULL,
  `tanggal_akhir_promo` date NOT NULL,
  `voucher` varchar(20) NOT NULL,
  `potongan` int(11) NOT NULL,
  `min_belanja` int(11) NOT NULL,
  `isi_promo` text NOT NULL,
  `id_ketersediaan` int(11) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promo`
--

INSERT INTO `promo` (`id`, `nama_promo`, `tanggal_mulai_promo`, `tanggal_akhir_promo`, `voucher`, `potongan`, `min_belanja`, `isi_promo`, `id_ketersediaan`, `gambar`) VALUES
(1, 'DISKON HARI RAYA NATAL DAN TAHUN BARU!', '0000-00-00', '2021-01-31', 'natalrotina', 10000, 50000, 'Herbs are fun and easy to grow. When harvested they make even the simplest meal seem like a gourmet delight. By using herbs in your cooking you can easily change the flavors of your recipes in many different ways, according to which herbs you add.', 1, 'IMG_9809.JPG'),
(4, 'HARI JADI TOMO FOOD', '2021-01-01', '2021-01-31', '', 0, 0, 'Beli 5 pcs tawar isi gratis 1 pcs', 1, 'IMG_98191.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id` int(11) NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `instagram` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `facebook`, `twitter`, `instagram`) VALUES
(1, 'rotina.isienak', 'ROTINA', 'rotina_tomofood');

-- --------------------------------------------------------

--
-- Table structure for table `statistik`
--

CREATE TABLE `statistik` (
  `ip` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `hits` int(11) NOT NULL,
  `online` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statistik`
--

INSERT INTO `statistik` (`ip`, `tanggal`, `hits`, `online`) VALUES
('::1', '2020-12-24', 62, '2020-12-24 20:28:58'),
('::1', '2020-12-25', 113, '2020-12-25 09:34:13'),
('::1', '2020-12-26', 273, '2020-12-26 12:19:01');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`) VALUES
(1, 'Belum Lunas'),
(2, 'Lunas');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `foto` text NOT NULL,
  `facebook` varchar(50) NOT NULL,
  `twitter` varchar(50) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  `youtube` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `nama`, `jabatan`, `foto`, `facebook`, `twitter`, `instagram`, `youtube`) VALUES
(1, 'RANDY BUTLER', 'Decorater', 'team-13.jpg', 'rotina', 'rotina', 'rotina', 'rotina'),
(2, 'RANDY BUTLER', 'Decorater', 'team-21.jpg', 'acquafres', 'rotina', 'rotina', 'rotina'),
(3, 'RANDY BUTLER', 'Decorater', 'team-3.jpg', 'acquafres', 'rotina', 'rotina', 'rotina'),
(4, 'RANDY BUTLER', 'Decorater', 'team-4.jpg', 'rotina', 'rotina', 'rotina', 'rotina'),
(5, 'RANDY BUTLER', 'Decorater', 'team-22.jpg', 'rotina', 'rotina', 'rotina', 'rotina'),
(7, 'RANDY BUTLER', 'Direktur', 'team-131.jpg', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tentang`
--

CREATE TABLE `tentang` (
  `id` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `visi` text NOT NULL,
  `misi` text NOT NULL,
  `petinggi` int(11) NOT NULL,
  `pekerja` int(11) NOT NULL,
  `tanggal_berdiri` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tentang`
--

INSERT INTO `tentang` (`id`, `deskripsi`, `visi`, `misi`, `petinggi`, `pekerja`, `tanggal_berdiri`) VALUES
(1, '<p style=\"margin: 0cm 0cm 7.5pt; line-height: 21pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"text-indent: 0cm;\">PT. Tomo Food Industri (TFI) didirikan pada tahun 2011. Lokasi pabrik\r\nberada di Desa Sakurjaya, Kecamatan Ujungjaya, Kabupaten Sumedang, Jawa Barat.\r\n</span></p><p style=\"margin: 0cm 0cm 7.5pt; line-height: 21pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"text-indent: 0cm;\">TFI memproduksi makanan (Khususnya Roti). </span></p><p style=\"margin: 0cm 0cm 7.5pt; line-height: 21pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"text-indent: 0cm;\">Tujuan</span><span style=\"text-indent: 0cm;\">  </span><span style=\"text-indent: 0cm;\">TFI memenuhi kebutuhan pangan Nasional\r\numumnya dan Pulau Jawa khususnya.</span><span style=\"text-indent: 0cm;\">  </span></p><p class=\"MsoNormal\" align=\"left\" style=\"margin-right: -5.65pt; margin-bottom: 6pt; margin-left: 0cm; text-indent: 0cm;\"><span lang=\"IN\"><o:p></o:p></span></p><p style=\"margin: 0cm 0cm 7.5pt; line-height: 21pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><br></p>', '<p>1. Bla bla bla Bla bla bla Bla bla bla Bla bla bla</p><p>2. Bla bla bla Bla bla bla Bla bla bla Bla bla bla</p><p>3. Bla bla bla Bla bla bla Bla bla bla Bla bla bla</p><p>4. Bla bla bla Bla bla bla Bla bla bla Bla bla bla</p>', '<p>1. Bla bla bla Bla bla bla Bla bla bla Bla bla bla</p><p>2. Bla bla bla Bla bla bla Bla bla bla Bla bla bla</p><p>3. Bla bla bla Bla bla bla Bla bla bla Bla bla bla</p><p>4. Bla bla bla Bla bla bla Bla bla bla Bla bla bla<br></p>', 1, 100, '2011-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `foto` text NOT NULL,
  `role` enum('admin','pembeli') NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `alamat`, `foto`, `role`, `tanggal`) VALUES
(1, 'superadmin', 'superadmin', '', '', '', 'admin', '0000-00-00'),
(2, 'wulandwi242', 'wulandwi242', 'Wulan Dwi Hartati', 'Jl. Rd Dewi Sartika No.16, Regol Wetan, Kec. Sumedang Sel., Kabupaten Sumedang, Jawa Barat 45311', 'wulandwi1.jpg', 'pembeli', '2020-12-25'),
(10, 'farhanm', 'farhanm', 'Farhan Maulana', 'Jl. Perintis Kemerdekaan No.93, Tugujaya, Kec. Cihideung, Tasikmalaya, Jawa Barat 46126, Indonesia. Bandung City View 1. Jl. Pasir Impun', '4__Foto_4x61.jpg', 'pembeli', '2020-12-25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bulan`
--
ALTER TABLE `bulan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instagram`
--
ALTER TABLE `instagram`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ketersediaan`
--
ALTER TABLE `ketersediaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tentang`
--
ALTER TABLE `tentang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bulan`
--
ALTER TABLE `bulan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ketersediaan`
--
ALTER TABLE `ketersediaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `promo`
--
ALTER TABLE `promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
